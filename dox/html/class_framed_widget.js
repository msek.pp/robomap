var class_framed_widget =
[
    [ "FramedWidget", "class_framed_widget.html#a1c6f4531fc17c8cd36c229a3a4f9175f", null ],
    [ "FramedWidget", "class_framed_widget.html#a257d943892d078be53e1ca628405c98b", null ],
    [ "~FramedWidget", "class_framed_widget.html#a40856bf6e5c17e55a9e35e24f1b9eb9f", null ],
    [ "setVLayout", "class_framed_widget.html#aef2985a3761269e6e076651131bc1fb6", null ],
    [ "setStoredWidget", "class_framed_widget.html#abcbce7540975ea1dee22fb0b8bb8109e", null ],
    [ "changeTitle", "class_framed_widget.html#a707169afc04bdd361d8060903ee94845", null ],
    [ "title", "class_framed_widget.html#ace4a4b291bdc10ef5c4599a43baf1ab5", null ],
    [ "vlayout", "class_framed_widget.html#a5b62f9e21c0b6f3e797070d26027ac45", null ]
];