var class_robot_data =
[
    [ "RobotData", "class_robot_data.html#a0007f4b014a950d356d2d63156b572af", null ],
    [ "getDistance", "class_robot_data.html#aee64ded96944fd7d61557f566a5c804f", null ],
    [ "getCounter", "class_robot_data.html#affa0add6183e662083841442a9976cb3", null ],
    [ "getVelocity", "class_robot_data.html#adc9b093e38bfe8ae8eaf3f70e5cacefa", null ],
    [ "returnDistance", "class_robot_data.html#a0e99308bd0a255942711d5a7f5cf9639", null ],
    [ "returnCounter", "class_robot_data.html#a1dbb990ef5d66e5a48651c30b0183fb7", null ],
    [ "returnVelocity", "class_robot_data.html#ac12e26952ade6fa132e489bc0c95b6c2", null ],
    [ "lock", "class_robot_data.html#a9995d73211afe3dc49fd451b2eb04c8b", null ],
    [ "sensors", "class_robot_data.html#a9a1648c46a58e390becfb98d78cbe491", null ],
    [ "ktir_counters", "class_robot_data.html#a3b322c493f67b40e3f118bddf4e0137c", null ],
    [ "motors_velocity", "class_robot_data.html#a93a9102961e0701b95e642ed3b2165e9", null ]
];