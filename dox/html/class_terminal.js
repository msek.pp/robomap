var class_terminal =
[
    [ "Terminal", "class_terminal.html#afba3c4445e1aa212be657a0460a9b070", null ],
    [ "getTimeString", "class_terminal.html#a2c2a517c4e7d243b2a9be48eed28c148", null ],
    [ "setVLayout", "class_terminal.html#ad54014a5ede6a6da9d3f8c3cc0dd393a", null ],
    [ "setStoredWidget", "class_terminal.html#a679df2b8a9473791a7190f180c06a1ee", null ],
    [ "setBackgroundColour", "class_terminal.html#ad775e6fcf056ddda51a674a3e3f43b11", null ],
    [ "typeSerialOpenError", "class_terminal.html#a965c70b8309ba765f8575b46c272c8cf", null ],
    [ "typeSerialReadError", "class_terminal.html#aaa6bdacda486c7f68835318acd0ab597", null ],
    [ "typeSerialReady", "class_terminal.html#abeee2c56557f7086f2212548c49fdc0e", null ],
    [ "typeSerialEnded", "class_terminal.html#ac7dee8173df7da33ac52dc399a2a2361", null ],
    [ "typeObstacleDetected", "class_terminal.html#a309e9e3b36e0af1fbf7df19a36ec58dd", null ],
    [ "typePlLang", "class_terminal.html#a18a4559d30c3e67c7d76964460aa0681", null ],
    [ "typeEngLang", "class_terminal.html#a7a46effe2bef072b6dfe72a52551edaa", null ],
    [ "editor", "class_terminal.html#adcf8c667948ef7bb6007c731976c4286", null ]
];