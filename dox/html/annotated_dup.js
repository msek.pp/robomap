var annotated_dup =
[
    [ "DistanceWin", "class_distance_win.html", "class_distance_win" ],
    [ "FramedWidget", "class_framed_widget.html", "class_framed_widget" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "Map3D", "class_map3_d.html", "class_map3_d" ],
    [ "NumericData", "class_numeric_data.html", "class_numeric_data" ],
    [ "RobotData", "class_robot_data.html", "class_robot_data" ],
    [ "RobotImage", "class_robot_image.html", "class_robot_image" ],
    [ "RobotModel", "class_robot_model.html", "class_robot_model" ],
    [ "SerialPort", "class_serial_port.html", "class_serial_port" ],
    [ "Terminal", "class_terminal.html", "class_terminal" ],
    [ "TravelInfo", "class_travel_info.html", "class_travel_info" ],
    [ "VelocityGraph", "class_velocity_graph.html", "class_velocity_graph" ],
    [ "Viewer", "class_viewer.html", "class_viewer" ]
];