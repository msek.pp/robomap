var class_travel_info =
[
    [ "TravelInfo", "class_travel_info.html#a5e690c384afedf00affae1abdb911e40", null ],
    [ "setHLayout", "class_travel_info.html#a2d74900881683d93e726b64fe1c9a710", null ],
    [ "setVLayout", "class_travel_info.html#abb606d890020bd082c842de4963f4ba4", null ],
    [ "setStoredWidget", "class_travel_info.html#af594443e14524f51ec5d8d83adbac2fe", null ],
    [ "writeLine", "class_travel_info.html#a7372d7b6b3cab2310b3e6482df168041", null ],
    [ "updateDistance", "class_travel_info.html#a3a7cf7c55842a5ca903c4cfdecc98921", null ],
    [ "updateCoords", "class_travel_info.html#a65eb7f9f08f585df6654f05238c9a526", null ],
    [ "setMaxWidth", "class_travel_info.html#a99fc19ffabbf99e2fd40d649a762e791", null ],
    [ "countCoords", "class_travel_info.html#aded556ed7e83a46c97f60e2593299ded", null ],
    [ "unit", "class_travel_info.html#a8dbb9fb85faaff85a6a4a648cdeed0b8", null ],
    [ "info", "class_travel_info.html#ad7d91725f7cbe294faa1b042872c4107", null ],
    [ "hlayout", "class_travel_info.html#a35c206edd5b89c207deb0f4e6ddb5203", null ],
    [ "coords", "class_travel_info.html#a708169e783d11f58c26c608e07864743", null ],
    [ "old_reads", "class_travel_info.html#a76cbff64d22110a5996cccfd1ed4ba1e", null ]
];