var class_velocity_graph =
[
    [ "VelocityGraph", "class_velocity_graph.html#abbf1a1c5050244e226995ee047dfbcff", null ],
    [ "setVLayout", "class_velocity_graph.html#a72f9c018ae8ceafa47a32b6b9a3238b0", null ],
    [ "setStoredWidget", "class_velocity_graph.html#a76c16fc021348ae060a8f29ffce2ebbf", null ],
    [ "setPlotGradient", "class_velocity_graph.html#a4b4031c030e02241414de91769278bc7", null ],
    [ "updateWheelVelocity", "class_velocity_graph.html#a6f38fb49ed1209990c97f6753c35ed52", null ],
    [ "graph", "class_velocity_graph.html#a8668d46b4caf67ae43eb3b8e12ca8a31", null ],
    [ "time_elapsed", "class_velocity_graph.html#a4e8f2ea12d76ea86f3d9fbbe164a49a3", null ]
];