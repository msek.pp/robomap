var hierarchy =
[
    [ "QFrame", null, [
      [ "FramedWidget", "class_framed_widget.html", [
        [ "DistanceWin", "class_distance_win.html", null ],
        [ "Map3D", "class_map3_d.html", null ],
        [ "Terminal", "class_terminal.html", null ],
        [ "TravelInfo", "class_travel_info.html", null ],
        [ "VelocityGraph", "class_velocity_graph.html", null ]
      ] ]
    ] ],
    [ "QGLViewer", null, [
      [ "Viewer", "class_viewer.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "NumericData", "class_numeric_data.html", null ],
      [ "RobotImage", "class_robot_image.html", null ]
    ] ],
    [ "RobotData", "class_robot_data.html", null ],
    [ "RobotModel", "class_robot_model.html", null ],
    [ "SerialPort", "class_serial_port.html", null ]
];