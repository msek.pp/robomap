#ifndef MAP3D_HPP
#define MAP3D_HPP

/*!
 * \file
 * \brief Definicja klasy Map3D
 *
 * Plik zawiera definicję klasy Map3D, która dziedziczy
 * po klasie FramedWidget i stanowi kontener na okno
 * wyświetlające obiekty 3D.
 */

#include "Viewer.hpp"
#include "FramedWidget.hpp"

/*!
 * \brief Opakowanie okna z obiektami trójwymiarowymi
 *
 * Dziedziczy po klasie FramedWidget, co nadaje całemu
 * obiektowi obramowanie oraz wprowadza tytuł okna.
 */
class Map3D : public FramedWidget
{
  private:
    /*!
     * \brief Okno do wyświetlania grafiki 3D
     */
    Viewer *map;
    /*!
     * \brief Kontener na okno z grafiką
     */
    QWidget *map_container;
    /*!
     * \brief Zarządca wertykalny
     */
    QVBoxLayout *map_container_layout;
    /*!
     * \brief Inicjalizuje widgety pochodne
     *
     * Metoda wirtualna klasy FramedWidget. Nadaje przechowywanym
     * widgetom odpowiednie ustawienia, wygląd, zależnie od danego
     * widgetu.
     */
    void setStoredWidget() override;
    /*!
     * \brief Ustawia widget'y pochodne w pionie
     *
     * Metoda wirtualna klasy FramedWidget.
     */
    void setVLayout() override;

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy Map3D
     */
    Map3D(const char *map_title, QWidget *parent = nullptr);
    /*!
     * \brief Przekazuje dane ruchu do modelu robota
     *
     * Funkcja pomocnicza, która ma na celu przekazanie prędkości
     * obu kół jak i przedziału czasowego (czas działania głównego timera).
     *
     * \param[in] left - wypełnienie PWM lewego koła (może być ujemne)
     * \param[in] right - wypełnienie PWM prawego kołą (może być ujemne)
     * \param[in] - time_interval - ułamek czasu, w którym robot się
     *              przemieszczał z powyższymi prędkościami
     */
    void moveRobot(int left, int right, int time_interval) { map->moveRobot(left, right, time_interval); }
    /*!
     * \brief Przekazuje robotowi odczyt z danego czujnika
     *
     * \param[in] new_measure - nowy odczyt z czujnika w [mm]
     * \param[in] index - indeks w tablicy czujników (numer czujnika
     *            0 - lewy, 1 - lewy przedni, 2 - prawy przedni, 3 - prawy)
     */
    void passMeasure(int new_measure, int index) { map->passMeasure(new_measure, index); }
    /*!
     * \brief Odświeża widok w oknie grafiki
     *
     * Wymusza wywołanie metody 'update()' klasy Viewer, co z kolei
     * powoduje ponowne odrysowanie elementów w oknie.
     */
    void refresh() { map->update(); }
    /*!
     * \brief Zwraca całkowitą odległość przebytą przez robota
     *
     * Przekazuje poziom wyżej przebytą przez robota drogę.
     *
     * \return przebyta odległość w [cm] - prwyatny atrybut klasy RobotModel
     */
    double returnRobotDistance() { return map->returnRobotDistance(); }
};

#endif // MAP3D_HPP
