#ifndef ROBOTDATA_HPP
#define ROBOTDATA_HPP

#include <mutex>

/*!
 * \file
 * \brief Definicja klasy RobotData
 *
 * Plik zawiera definicję klasy RobotData, która
 * przechowywuje wszystkie dane otrzymywane z robota.
 */

/*!
 * \brief Zbiór wszystkich danych otrzymywanych z robota
 */
class RobotData
{
  private:
    std::mutex lock;

    /*!
     * \brief Tablica z odczytami z czujników odległości
     */
    unsigned int sensors[4];
    /*!
     * \brief Tablica z odczytami transoptorów odbiciowych
     */
    unsigned int ktir_counters[2];
    /*!
     * \brief Tablica z prędkościami silników (wypełnienie sygnału PWM)
     */
    int motors_velocity[2];
    /*!
     * \brief Inicjalizuje obiekt RobotData
     */

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy RobotData
     */
    RobotData();
    /*!
     * \brief Aktualizuje dane z czujników odległości
     *
     * \param[in] distance - nowy odczyt
     * \param[in] index - indeks tablicy / numer czujnika
     */
    void getDistance(unsigned int distance, int index)
    {
        std::lock_guard<std::mutex> safe_guard(lock);
        sensors[index] = distance;
    }
    /*!
     * \brief Aktualizuje dane z transoptorów odbiciowych
     *
     * \param[in] counter - nowa liczba przejechanych pól planszy
     * \param[in] index - indeks tablicy / numer transoptora
     */
    void getCounter(unsigned int counter, int index)
    {
        std::lock_guard<std::mutex> safe_guard(lock);
        ktir_counters[index] = counter;
    }
    /*!
     * \brief Aktualizuje prędkości silników
     *
     * \param[in] velocity - nowa prędkość (wypełnienie PWM)
     * \param[in] index - indeks tablicy / numer silnika
     */
    void getVelocity(int velocity, int index)
    {
        std::lock_guard<std::mutex> safe_guard(lock);
        motors_velocity[index] = velocity;
    }
    /*!
     * \brief Zwraca odległość danego czujnika
     *
     * \param[in] index - indeks tablicy / numer czujnika
     *
     * \return odległość wyrażona w [mm]
     */
    unsigned int returnDistance(int index)
    {
        std::lock_guard<std::mutex> safe_guard(lock);
        return sensors[index];
    }
    /*!
     * \brief Zwraca wartość danego transoptora odbiciowego
     *
     * \param[in] index - indeks tablicy / numer transoptora (0 - lewy, 1 - prawy)
     *
     * \return cyfrowa wartość napięcia na fototranzystorze
     */
    unsigned int returnCounter(int index)
    {
        std::lock_guard<std::mutex> safe_guard(lock);
        return ktir_counters[index];
    }
    /*!
     * \brief Zwraca wypełnienie PWM danego koła
     *
     * \param[in] index - indeks tablicy / numer koła (0 - lewe, 1 - prawe)
     *
     * \return wypełnienie sygnału PWM (może być ujemne, co oznacza obrót
     *         w przeciwną stronę)
     */
    unsigned int returnVelocity(int index)
    {
        std::lock_guard<std::mutex> safe_guard(lock);
        return motors_velocity[index];
    }
};

#endif // ROBOTDATA_HPP
