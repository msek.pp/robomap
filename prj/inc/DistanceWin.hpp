#ifndef DISTANCEWIN_HPP
#define DISTANCEWIN_HPP

/*!
 * \file
 * \brief Definicja klasy DistanceWin
 *
 * Plik zawiera definicję klasy DistanceWin, która
 * dziedziczy klasę FramedWidget. Wyświetla odczyty
 * czujników odległości kładzione na rzut z góry
 * modelu 3D robota.
 */

#include "FramedWidget.hpp"
#include "NumericData.hpp"
#include "RobotImage.hpp"

#define SENSORS 4
#define WIN_HEIGHT 320

/*!
 * \brief Widget wyświetlający odczyty czujników odległości
 *
 * Klasa łączy ze sobą obiekty różnego typu wyświetlając je w
 * jednym oknie. Dziedziczy ona po klasie FramedWidget, co
 * powoduje obramowanie widgetu. Odczyty czujników rysowane są
 * na planie siatki atrybutu QGridLayout, która nałożona jest
 * na obraz robota - atrybut RobotImage.
 */
class DistanceWin : public FramedWidget
{
  Q_OBJECT
  private:
    /*!
     * \brief Tablica odczytów z czujników odległości
     *
     * Klasa wyświetla odczyty z dokładnością do 1 milimetra.
     * Więcej o klasie NumericData.
     */
    NumericData *sensors_data[SENSORS];
    /*!
     * \brief Zarządca geometrii na planie siatki
     *
     * Układa dane z sensorów w taki sposób, aby były
     * blisko odpowiednich czujników na obrazie robota
     * (łatwe skojarzenie, który odczyt dotyczy którego czujnika).
     */
    QGridLayout *image_grid;
    /*!
     * \brief Obraz robota
     *
     * Wyświetla obraz w widgecie. Klasa RobotImage dba o odpowiednie
     * rozmiary obrazu i ich dynamiczne zmiany, reimplementując
     * metodę painEvent().
     */
    RobotImage *image;
    /*!
     * \brief Ustawia widgety pochodne w pionie
     *
     * Metoda wirtualna klasy FramedWidget.
     */
    void setVLayout() override;
    /*!
     * \brief Inicjalizuje widgety pochodne
     *
     * Metoda wirtualna klasy FramedWidget. Nadaje przechowywanym
     * widgetom odpowiednie ustawienia, wygląd, zależnie od danego
     * widgetu.
     */
    void setStoredWidget() override;

  public:
    /*!
     * \brief Inicjalizuje widget DistanceWin
     */
    DistanceWin(const char *dist_title, QWidget *parent = nullptr);
    /*!
     * \brief Aktualizuje odczyty z czujników odległości
     *
     * Wywołuje metodę update() klasy NumericData odpowiedniego obiektu
     * z tablicy sensors_Data[].
     *
     * \param[in] new_dist - nowy odczyt z czujnika
     * \param[in] index - indeks obiektu z tablicy sensors_data[] (numer czujnika)
     */
    void updateDistances(unsigned int new_dist, int index) { sensors_data[index]->update(new_dist); }
};

#endif // DISTANCEWIN_HPP
