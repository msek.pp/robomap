#ifndef ROBOTMODEL_HPP
#define ROBOTMODEL_HPP

/*!
 * \file
 * \brief Definicja klasy RobotModel
 *
 * Plik zawiera definicję klasy RobotModel, która
 * reprezentuje trójwymiarowy model robota, jak i
 * jego zachowanie (model kinematyki, orientacja aktualne
 * współrzędne). Poza tym zajmuje się też pozostałymi
 * aspektami wizualizacji 3D, takimi jak rysowanie przeszkód
 * czy planszy. Posiada prototypy funkcji pomocnych w pro-
 * cesie rysowania.
 */

#include <QGLViewer/qglviewer.h>

// Długość planszy
#define BOARD_X 60
// Szerokość planszy
#define BOARD_Y 42
// Zewnętrzny promień koła robota
#define WHEEL_RADIUS_EXT 1.45F
// Wewnętrzny promień koła robota
#define WHEEL_RADIUS_IN 0.75F
// Szerokość koła robota
#define WHEEL_WIDTH 1.3F
// Dokładność odrysowywanych okręgów
#define ANG_STEP_DEG 10
// Odległość pomiędzy kołami robota
#define WHEEL_SPACING 8.6F
// Wysokość rysowanych przeszkód
#define OBSTACLE_HEIGHT 2.1F
// Współczynnik kierunkowy regresji liniowej
#define LINEAR_COEF 9.85
// Wyraz wolny regresji liniowej
#define OFFSET 7.395194838
// Maksymala wielkość bufora ze współrzędnymi przeszkód
#define MAX_BUF_SIZE 1000

// ~~~~~~~ Stałe dotyczące czujników odległości
// Kąt między orietnacją lewego i prawego czujnika a prostą
// prostopadłą do poziomej linii podstawy robota
#define ANGLE 34
// Przesunięcie w osi X lewego i prawego czujnika od punktu
// między kołami robota
#define OFFSETX1 4.45
// Przesunięcie w osi Y lewego i prawego czujnika od punktu
// między kołami robota
#define OFFSETY1 4.05
// Przesunięcie w osi X przednich czujników od punktu między
// kołami robota
#define OFFSETX2 5.65
// Przesunięcie w osi Y przednich czujników od punktu między
// kołami robota
#define OFFSETY2 1.4
// Promień okręgu detekcji (w jakiej odległości robot zauważa przeszkody)
#define DETECTION_RADIUS 25.0F

/*!
 * \brief Trójwymiarowy model robota
 *
 * Klasa ta określa reguły budowania modelu składającego się
 * z dwóch kół, dwóch silników, podstawy, baterii oraz przedniej
 * ściany spychającej. Definiuje kolory poszczególnych elementów,
 * zachowanie robota na planszy, reguły wizualizowania przeszkód.
 */
class RobotModel
{
  private:
    /*!
     * \brief Wymiary podstawy robota w osi X
     *
     * Ściślej mówiąc, są to charakterystyczne współrzędne
     * osi X symetrycznego odbicia podstawy robota.
     */
    float base_xdim[4] = {0, 3.1, 8.4, 9.9};
    /*!
     * \brief Wymiary podstawy robota w osi Y
     *
     * Współrzędne osi Y symetrycznego odbicia podstawy
     * robota. Aby jednoznacznie określić punkty podstawy,
     * potrzebne są współrzędne osi X oraz osi Y dodatnie i
     * ujemne.
     */
    float base_ydim[3] = {3.4, 4.9, 2.8};
    /*!
     * \brief Kolor podstawy robota
     */
    const float base_color[3] = {0.1, 0.1, 0.1};
    /*!
     * \brief Kolor przedniej ściany robota
     */
    const float front_color[3] = {0.8, 0.8, 0.8};
    /*!
     * \brief Kolor rysowanych przeszkód
     */
    const float obstacle_color[3] = {0.9, 0.1, 0.1};
    /*!
     * \brief Współrzędne aktualnego położenia robota
     *
     * Pierwsza współrzędna X mówi, że startowe miejsce
     * robota to początek planszy z uwzględnieniem przesunięcia
     * do centralnego punktu dla modelu kinematyki (między kołami).
     */
    double position[2] = {(BOARD_X/2) - 1.55, 0.0};
    /*!
     * \brief Aktualna orientacja robota
     */
    double orientation = 0;
    /*!
     * \brief Początkowy kąt rysowania lewego koła
     *
     * Potrzebny do animacji jego obrotu, gdy robot się
     * przemieszcza.
     */
    double lwheel_rot = 0;
    /*!
     * \brief Początkowy kąt rysowania prawego koła
     *
     * Potrzebny do animacji jego obrotu, gdy robot się
     * przemieszcza.
     */
    double rwheel_rot = 0;
    /*!
     * \brief Całkowita droga przebyta przez robota
     */
    double distance = 0;
    /*!
     * \brief Pomiary czujników odległości
     *
     * Dynamiczna tablica dwuwymiarowa z pomiarami czujników
     * odległości. Maksymalnie zbieranych jest 8 pomiarów, na
     * których podstawie potem jest decydowana stabilność pomiaru
     * (robot stoi i mierzy) a sam wynik jest uśredniany.
     */
    std::vector<int> measures[4];
    /*!
     * \brief Współrzędne przeszkód
     *
     * Dynamiczna tablica współrzędnych zauważonych przeszkód, które
     * są potem odrysowywane z każdym wywołaniem metody "build()" nieza-
     * leżnie od położenia robota.
     */
    std::vector<std::pair<float, float>> obstacles;
    /*!
     * \brief Rysuje planszę, po której jeździ robot
     */
    void drawBoard();
    /*!
     * \brief Rysuje koło robota
     */
    void drawWheel(double rotation);
    /*!
     * \brief Rysuje podstawę robota
     */
    void drawBase();
    /*!
     * \brief Rysuje kompletną przednią ścianę robota
     */
    void drawFront();
    /*!
     * \brief Rysuje lewą i prawą część ściany
     */
    void drawFrontWing();
    /*!
     * \brief Rysuje frontową część ściany
     */
    void drawFrontWall();
    /*!
     * \brief Rysuje kompletny silnik DC
     */
    void drawMotor();
    /*!
     * \brief Rysuje złote blaszki silnika DC
     */
    void drawMotorFront();
    /*!
     * \brief Rysuje cienkie tuleje silnika
     */
    void drawMotorRoller();
    /*!
     * \brief Rysuje główną część silnika
     */
    void drawMotorMain();
    /*!
     * \brief Rysuje baterię
     */
    void drawBattery();
    /*!
     * \brief Rysuje okrąg detekcji robota
     */
    void drawDetectionCircle();
    /*!
     * \brief Sprawdza czy zebrano wystarczającą ilość pomiarów
     */
    bool checkMeasuresSize();
    /*!
     * \brief Zwraca uśredniony wynik, o ile ostatnie pomiary były stabilne
     */
    int returnMeasureIfConst(int index);

  public:
    /*!
     * \brief Konstruktor domyślny klasy
     */
    RobotModel() {  }
    /*!
     * \brief Rysuje wszystkie elementy mapy 3D
     */
    void build();
    /*!
     * \brief Oblicza nowe położenie i orietnację robota
     */
    void move(int left_velocity, int right_velocity, int time_interval);
    /*!
     * \brief Oblicza współrzędne przeszkód
     */
    void drawObstacle();
    /*!
     * \brief Rysuje kompletnego robota
     */
    void buildRobot();
    /*!
     * \brief Dodaje nowy pomiar z danego czujnika
     *
     * \param[in] new_measure - nowy pomiar z czujnika
     * \param[in] index - indeks / numer czujnika, do którego ten
     *            pomiar należy (0 - lewy, 1 - pierwszy przedni, 2 -
     *            drugi przedni, 3 - prawy)
     */
    void getMeasure(int new_measure, int index) { measures[index].push_back(new_measure); }
    /*!
     * \brief Zwraca odległość przebytą przez robota
     *
     * \return przebyta odległość w [cm]
     */
    double returnRobotDistance() { return distance; }
};

// ~~~~~~~~~~~~~ Funkcje pomocnicze ~~~~~~~~~~~~~ //
/*!
 * \brief Rysuje czarne pola na planszy (szachownicy)
 */
void drawBlackField(GLfloat x_position, GLfloat y_position, GLfloat z_position = 0.51);
/*!
 * \brief Rysuje prostopadłościan na podstawie danych współrzędnych
 */
void drawPolygon(GLfloat *x_coords,
                 GLfloat *y_coords,
                 GLfloat *z_coords,
                 const GLfloat *color = nullptr,
                 const GLfloat *norm = nullptr);
/*!
 * \brief Rysuje przeszkodę w postaci walca
 */
void drawObstacleLine(std::pair<float, float> coords, const float *color);
/*!
 * \brief Przekształca stopnie do radianów
 *
 * \param[in] angle_deg - kąt w stopniach
 * \return kąt w radianach
 */
inline
double deg2rad(double angle_deg) { return angle_deg*M_PI/180; }
/*!
 * \brief Przekształca radiany w stopnie
 *
 * \param[in] angle_rad - kąt w radianach
 * \return kąt w stopniach
 */
inline
double rad2deg(double angle_rad) { return angle_rad*180/M_PI; }
/*!
 * \brief Aktualizuje współrzędne w tablicy
 */
void updateArray(float *array, float point1, float point2, float point3, float point4);

#endif // ROBOTMODEL_HPP
