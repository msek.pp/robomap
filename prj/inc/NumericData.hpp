#ifndef NUMERICDATA_HPP
#define NUMERICDATA_HPP

/*!
 * \file
 * \brief Definicja klasy NumericData
 *
 * Plik zawiera definicję klasy NumericData, która
 * odpowiada za wyświetlanie danych z czujnika
 * odległości.
 */

#include <QLabel>
#include <QLineEdit>
#include <QLayout>

#define TEXT_LIMIT 10

/*!
 * \brief Wyświetla dane z czujnika odległości
 *
 * Dziedziczy po klasie QWidget (bez obramowania), aby
 * dane mogły zostać nałożone na inne obiekty (klasa DistanceWin).
 * Do wyświetlenia odczytów posługuje się obiektem klasy QLineEdit.
 */
class NumericData : public QWidget
{
  private:
    /*!
     * \brief Okienko do wyświetlania sformatowanych danych
     */
    QLineEdit *data;
    /*!
     * \brief Zarządca geometrii horyzontalnej
     */
    QHBoxLayout *hlayout;
    /*!
     * \brief Inicjalizuje widgety pochodne
     */
    void setStoredWidget();
    /*!
     * \brief Ustawia widgety w poziomie
     */
    void setHLayout();

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy NumericData
     */
    NumericData(QWidget *parent = nullptr);
    /*!
     * \brief Aktualizuje odczyt z czujnika odleŋłości
     */
    void update(unsigned int new_value);
};

#endif // NUMERICDATA_HPP
