#ifndef VELOCITYGRAPH_HPP
#define VELOCITYGRAPH_HPP

/*!
 * \file
 * \brief Definicja klasy VelocityGraph
 *
 * Plik zawiera definicję klasy VelocityGraph, która
 * dziedziczy klasę FramedWidget. Służy do wyświetlenia
 * wykresów prędkości kół w czasie.
 */

#include "qcustomplot.hpp"
#include "FramedWidget.hpp"

#include <iostream>

#define GRAPH_HEIGHT 150
#define MAX_RANGE 100.0

/*!
 * \brief Wykres prędkości koła w czasie
 *
 * Klasa VelocityGraph bazuje na działaniu klasy
 * QCustomPlot, która odpowiada za rysowanie wykresu.
 * W rzeczywistości rysowane jest wypełnienie sygnału
 * PWM każdego z silników. Wartość jest ujemna, jeżeli
 * robot jedzie do tyłu i dodatnia gdy jedzie do przodu.
 * Zakres wartości wykresu to <-1, 1>.
 */
class VelocityGraph : public FramedWidget
{
  Q_OBJECT
  private:
    /*!
     * \brief Wykres 2D
     */
    QCustomPlot *graph;
    /*!
     * \brief Aktualny odcinek czasu między pomiarami
     */
    double time_elapsed = 0;
    /*!
     * \brief Ustawia widgety pochodne w pionie
     *
     * Metoda wirtualna klasy FramedWidget.
     */
    void setVLayout() override;
    /*!
     * \brief Inicjalizuje widgety pochodne
     *
     * Metoda wirtualna klasy FramedWidget. Nadaje przechowywanym
     * widgetom odpowiednie ustawienia, wygląd, zależnie od danego
     * widgetu.
     */
    void setStoredWidget() override;
    /*!
     * \brief Ustawia gradient tła wykresu
     */
    void setPlotGradient();

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy VelocityGraph
     */
    VelocityGraph(const char *graph_title, QWidget *parent = nullptr);
    /*!
     * \brief Aktualizuje dane na wykresie i przesuwa osie zgodnie z czasem
     */
    void updateWheelVelocity(int velocity, int time_interval_ms);
};

#endif // VELOCITYGRAPH_HPP
