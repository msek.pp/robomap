#ifndef ROBOTIMAGE_HPP
#define ROBOTIMAGE_HPP

/*!
 * \file
 * \brief Definicja klasy RobotImage
 *
 * Plik zawiera definicję klasy RobotImage, która
 * dziedziczy klasę QWidget. Wyświetla rzut z góry
 * modelu 3D robota.
 */

#include <QPainter>
#include <QWidget>

#define SENSORS 4
#define IMAGE "/home/higgs_001/Robomap/prj/ui/RobotSumo.png"

/*!
 * \brief Widget wyświetlający obraz robota
 *
 * Dzięki reimplementacji metody paintEvent, rysowany na sobie
 * obraz jest dynamicznie dopasowywany do zmieniających się
 * rozmiarów rodzica.
 */
class RobotImage : public QWidget
{
  Q_OBJECT
  public:
    /*!
     * \brief Inicjalizuje obiekt klasy RobotImage
     */
    RobotImage(QWidget *parent = nullptr);
    /*!
     * \brief Rysuje obraz robota
     */
    virtual void paintEvent(QPaintEvent *event) override;
};

#endif // ROBOTIMAGE_HPP
