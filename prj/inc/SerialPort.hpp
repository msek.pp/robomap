#ifndef SERIALPORT_HPP
#define SERIALPORT_HPP

/*!
 * \file
 * \brief Definicja klasy SerialPort
 *
 * Plik zawiera definicję klasy SerialPort, która
 * dziedziczy po klasie QSerialPort. Obiekt skupia
 * się na dostarczeniu nowych slotów potrzebnych
 * do komunikacji z robotem.
 */

#include <string>
#include <iostream>
#include <condition_variable>
#include <sstream>
#include <cstring>
#include <vector>
#include <fcntl.h>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "RobotData.hpp"
#include "odbieranie.hh"
#include "transparam.hh"

#define FRAME_LIMIT 50

extern std::condition_variable start;
extern std::mutex cm;

/*!
 * \brief Modeluje port szeregowy
 *
 * Klasa SerialPort dodaje nowe sloty do klasy
 * QSerialPort, które są implementowane z myślą
 * o komunikacji z robotem.
 */
class SerialPort
{
  private:
    /*!
     * \brief Dane z robota
     *
     * Obiekt przechowywujący wszystkie dane otrzymywane od robota.
     */
    RobotData data;
    /*!
     * \brief Zmienna decydująca o działaniu wątku
     */
    bool keep_reading = false;
    /*!
     * \brief Deskryptor pliku portu szeregowego
     */
    int port_desc = -1;
    /*!
     * \brief Zwraca deskryptor pliku
     *
     * \return Liczba symbolizująca deskryptor
     */
    int returnDecriptor() const { return port_desc; }

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy SerialPort
     */
    SerialPort();
    /*!
     * \brief Metoda decydująca o kontynuacji czytania
     *
     * \return True dla dalszego czytania lub False dla przerwania
     */
    bool keepReading() { return keep_reading; }
    /*!
     * \brief Odbiera dane z portu
     *
     * \return True jeżeli wczytanie się powiodło lub False jeżeli nie
     */
    bool receiveData();
    /*!
     * \brief Otwiera port szeregowy
     */
    bool startConnection();
    /*!
     * \brief Kończy połączenie z portem szeregowym
     */
    bool endConnection();
    /*!
     * \brief Zwraca wskaźnik do obiektu z danymi robota
     *
     * \return Adres obiektu z danymi
     */
    RobotData * returnRobotData() { return &data; }
};

/*!
 * \brief Analizuje otrzymaną ramkę danych
 */
bool parseDataFrame(const char *data_frame, RobotData *data, int data_length);
/*!
 * \brief Sprawdza zgodność sumy kontrolnej CRC
 */
bool checkCRC(char *data_frame, int data_length, char crc);
/*!
 * \brief Oblicza sumę kontrolną otrzymanej ramki danych
 */
char crc8(char *data_frame, int data_length);
/*!
 * \brief Funkcja wykonywana przez wątek czytający
 */
void process(SerialPort *serial);

#endif // SERIALPORT_HPP
