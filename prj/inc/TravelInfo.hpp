#ifndef TRAVELINFO_HPP
#define TRAVELINFO_HPP

/*!
 * \file
 * \brief Definicja klasy TravelInfo
 *
 * Plik zawiera definicję klasy TravelInfo, która
 * dziedziczy klasę FramedWidget. Służy do wyświetlania
 * informacji o przebytej drodze oraz o aktualnych
 * współrzędnych robota.
 */

#include "FramedWidget.hpp"
#include <QLineEdit>

#define TRAVEL_INFO_LIMIT 16

/*!
 * \brief Wyświetla informacje o podróży robota
 *
 * Klasa ma za zadanie wyświetlać
 * informacje o przebytej drodze oraz o aktualnych
 * współrzędnych robota, jednakże dzięki zastosowaniu
 * obiektów QLabel oraz QLineEdit, może być wykorzystana
 * do wyświetlenia różnego typu informacji.
 */
class TravelInfo : public FramedWidget
{
  Q_OBJECT
  private:
    /*!
     * \brief Jednostka wyświetlanych danych (milimetry)
     */
    QLabel *unit;
    /*!
     * \brief Miejsce na wpisywaną informację
     */
    QLineEdit *info;
    /*!
     * \brief Zarządca horyzontalny
     */
    QHBoxLayout *hlayout;
    /*!
     * \brief Tablica z koordynatami robota
     */
    int coords[2] = {0, 0};
    /*!
     * \brief Tablica z poprzednimi odczytami z transoptorów odbiciowych
     *
     * Potrzebna do prawidłowego aproksymowania aktualnych współrzędnych
     * na podstawie danych z transoptorów.
     */
    int old_reads[2];
    /*!
     * \brief Ustawia widgety w poziomie
     */
    void setHLayout();
    /*!
     * \brief Ustawia widgety pochodne w pionie
     *
     * Metoda wirtualna klasy FramedWidget.
     */
    void setVLayout() override;
    /*!
     * \brief Inicjalizuje widgety pochodne
     *
     * Metoda wirtualna klasy FramedWidget. Nadaje przechowywanym
     * widgetom odpowiednie ustawienia, wygląd, zależnie od danego
     * widgetu.
     */
    void setStoredWidget() override;

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy TravelInfo
     */
    TravelInfo(const char *widget_title, QWidget *parent = nullptr);
    /*!
     * \brief Wpisuje linijkę do edytora
     */
    void writeLine(QString line);
    /*!
     * \brief Konweruje przekazany dystans na metry i wyświetla w okienku
     */
    void updateDistance(double distance);
    /*!
     * \brief Aktualizuje współrzędne robota w okienku
     */
    void updateCoords();
    /*!
     * \brief Ustawia maksymalną szerokość edytora
     *
     * \param[in] limit - maskymalna szerokość
     */
    void setMaxWidth(int limit = 100) { info->setMaximumWidth(limit); }
    /*!
     * \brief Liczy współrzędne robota na planszy
     */
    void countCoords(int left_opto, int right_opto);
};

#endif // TRAVELINFO_HPP
