#ifndef VIEWER_HPP
#define VIEWER_HPP

/*!
 * \file
 * \brief Definicja klasy Viewer
 *
 * Plik zawiera definicję klasy Viewer, która dziedziczy
 * po klasie QGLViewer. Służy ona do wyświetlania trój-
 * wymiarowych obiektów tworzonych z pomocą openGL
 * (plansza, robot przeszkody).
 */

#include "RobotModel.hpp"

/*!
 * \brief Okno z modelami trójwymiarowymi
 *
 * Odpowiada za rysowanie wszystkich obiektów od nowa
 * (odświeża mapę). Jest pośrednikiem między widgetem
 * w obiekcie MainWindow a modelem robota.
 */
class Viewer : public QGLViewer
{
  private:
    /*!
     * \brief Trójwymiarowy model robota
     */
    RobotModel robot;

  public:
    /*!
     * \brief Tworzy obiekt typu Viewer
     *
     * Rozmiar sceny ograniczany jest do półkuli o promieni
     * 25.
     *
     * \param[in] parent - wskaźnik na rodzica
     */
    Viewer(QWidget *parent = nullptr) : QGLViewer(parent) { setSceneRadius(25); }
    /*!
     * \brief Przekazuje dane ruchu do modelu robota
     *
     * Funkcja pomocnicza, która ma na celu przekazanie prędkości
     * obu kół jak i przedziału czasowego (czas działania głównego timera).
     *
     * \param[in] left - wypełnienie PWM lewego koła (może być ujemne)
     * \param[in] right - wypełnienie PWM prawego kołą (może być ujemne)
     * \param[in] time_interval - ułamek czasu, w którym robot się
     *              przemieszczał z powyższymi prędkościami
     */
    void moveRobot(int left, int right, int time_interval) { robot.move(left, right, time_interval); }
    /*!
     * \brief Przekazuje robotowi odczyt z danego czujnika
     *
     * \param[in] new_measure - nowy odczyt z czujnika w [mm]
     * \param[in] index - indeks w tablicy czujników (numer czujnika
     *            0 - lewy, 1 - lewy przedni, 2 - prawy przedni, 3 - prawy)
     */
    void passMeasure(int new_measure, int index) { robot.getMeasure(new_measure, index); }
    /*!
     * \brief Zwraca odległość przebytą przez robota
     *
     * \return przebyta odległość w [cm] - prwyatny atrybut klasy RobotModel
     */
    double returnRobotDistance() { return robot.returnRobotDistance(); }

  protected:
    /*!
     * \brief Funkcja rysująca obiekty
     */
    virtual void draw() override;
    /*!
     * \brief Funkcja inicjalizująca okno do wyświetlania grafiki
     */
    virtual void init() override;
};

#endif // VIEWER_HPP
