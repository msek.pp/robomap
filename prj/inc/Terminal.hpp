#ifndef TERMINAL_HPP
#define TERMINAL_HPP

/*!
 * \file
 * \brief Definicja klasy Terminal
 *
 * Plik zawiera definicję klasy Terminal, która
 * dziedziczy klasę FramedWidget. Służy do
 * wyświetlania różnego typu komunikatów w trakcie
 * działania programu.
 */

#include <QTextEdit>
#include <QDateTime>
#include "FramedWidget.hpp"

/*!
 * \brief Wyświetla różnego typu komunikaty
 *
 * Klasa Terminal bazuje na działaniu obiektu
 * QTextEditor, który pracuje w trybie ReadOnly.
 * Odbiera sygnały z różnych części aplikacji i
 * reaguje na nie wyświetlając odpowiedni komunikat
 * informujący użytkownika o stanie programu. Każdemu
 * komunikatowi towarzyszy godzina wysłania.
 */
class Terminal : public FramedWidget
{
  Q_OBJECT
  private:
    /*!
     * \brief Wyświetla komunikaty
     *
     * Gdy komunikatów będzie za dużo, obiekt klasy
     * QTextEdit dba o mechanizm suwaka i możliwość
     * przeglądu starszych komunikatów.
     */
    QTextEdit *editor;
    /*!
     * \brief Zwraca aktualny czas w formie tekstu
     */
    const QString getTimeString();
    /*!
     * \brief Ustawia widgety pochodne w pionie
     *
     * Metoda wirtualna klasy FramedWidget.
     */
    void setVLayout() override;
    /*!
     * \brief Inicjalizuje widgety pochodne
     *
     * Metoda wirtualna klasy FramedWidget. Nadaje przechowywanym
     * widgetom odpowiednie ustawienia, wygląd, zależnie od danego
     * widgetu.
     */
    void setStoredWidget() override;

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy Terminal
     */
    Terminal(const char *terminal_title, QWidget *parent = nullptr);
    /*!
     * \brief Ustawia tło edytora
     */
    void setBackgroundColour(const QPalette & palette);

  public slots:
    /*!
     * \brief Wyświetla komunikat o błędzie otworzenia portu szeregowego
     */
    void typeSerialOpenError();
    /*!
     * \brief Wyświetla komunikat o błędzie wczytania danych
     */
    void typeSerialReadError();
    /*!
     * \brief Wyświetla komunikat o prawidłowej konfiguracji portu
     */
    void typeSerialReady();
    /*!
     * \brief Wyświetla komunikat o zakończeniu połączenia z portem
     */
    void typeSerialEnded();
    /*!
     * \brief Wyświetla komunikat o wykryciu przeszkody
     */
    void typeObstacleDetected();
    /*!
     * \brief Wyświetla komunikat o zmianie języka na polski
     */
    void typePlLang();
    /*!
     * \brief Wyświetla komunikat o zmianie języka na angielski
     */
    void typeEngLang();
};

#endif // TERMINAL_HPP
