#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

/*!
 * \file
 * \brief Definicja klasy MainWindow
 *
 * Plik zawiera definicję klasy MainWindow, która
 * dziedziczy po klasie QMainWindow. Zawiera w sobie
 * wszystkie najważniejsze widgety okna aplikacji.
 */

#include <QMainWindow>
#include <QPushButton>
#include <QTimer>
#include <QThread>
#include <QMenuBar>
#include "Terminal.hpp"
#include "DistanceWin.hpp"
#include "VelocityGraph.hpp"
#include "Map3D.hpp"
#include "RobotData.hpp"
#include "SerialPort.hpp"
#include "TravelInfo.hpp"

#define WIDGETS_WIDTH 550
#define WIDGETS_XCOORD 320
#define XMARGIN 20
#define YMARGIN 30
#define TIME_INTERVAL 50

extern std::condition_variable start;
extern std::mutex cm;

/*!
 * \brief Główne okno aplikacji
 *
 * Tworzy i zarządza wszystkimi głównymi widgetami
 * aplikacji. Odpowiada za aktualizację danych
 * otrzymanych z robota w każdym widgecie. Uruchamia
 * pracę apikacji oraz kończy jej działanie wykorzystując
 * do tego dwa przyciski.
 */
class MainWindow : public QMainWindow
{
  Q_OBJECT
  private:
    /*!
     * \brief Główny widget okna aplikacji
     *
     * Rozciągnięty na cały obszar okna, przechowywuje
     * wszystkie elementy okna aplikacji, w tym zarządców
     * geometrii.
     */
    QWidget *main_widget;
    /*!
     * \brief Menu aplikacji
     */
    QMenu *lang_menu;
    /*!
     * \brief Akcja związana z wyborem języka polskiego
     */
    QAction *pl_lang;
    /*!
     * \brief Akcja związana z wyborem języka angielskiego
     */
    QAction *eng_lang;
    /*!
     * \brief Przycisk rozpoczynający pracę aplikacji
     */
    QPushButton *start_button;
    /*!
     * \brief Przycisk kończący pracę apliklacji
     */
    QPushButton *end_button;
    /*!
     * \brief Okno terminala (Terminal)
     */
    Terminal *terminal;
    /*!
     * \brief Trójwymiarowa mapa robota (Map3D)
     */
    Map3D *map;
    /*!
     * \brief Dwa wykresy prędkości (VelocityGraph)
     */
    VelocityGraph *graphs[2];
    /*!
     * \brief Okno z odczytami czujników odległości (DistanceWin)
     */
    DistanceWin *distances;
    /*!
     * \brief Okno z informacją o przebytej drodze (TravelInfo)
     */
    TravelInfo *traveled_distance;
    /*!
     * \brief Okno z informacją o aktualnych współrzędnych robota (TravelInfo)
     */
    TravelInfo *coords;
    /*!
     * \brief Klasa obsługująca port szeregowy
     */
    SerialPort *serial;
    /*!
     * \brief Stoper wyznaczający takt czytaania portu szeregowego
     */
    QTimer *timer;
    /*!
     * \brief Zarządca wertykalny (prawa kolumna okna aplikacji)
     */
    QVBoxLayout *vertical_layout;
    /*!
     * \brief Zarządca horyzontalny aplikacji (mapa + zarządca wertykalny)
     */
    QHBoxLayout *horizon_layout;
    /*!
     * \brief Zarządca horyzontalny układający informacje o podróży robota
     *
     * Poddany jest on pod zarządcę wertykalnego prawej kolumny okna aplikacji
     */
    QHBoxLayout *travelinfo_layout;
    /*!
     * \brief Inicjalizuje menu aplikacji (wybór języka)
     */
    void createMenu();
    /*!
     * \brief Rozwija menu kontekstowe
     *
     * \param[in] event
     */
    void contextMenuEvent(QContextMenuEvent *event) override;
    /*!
     * \brief Metoda tłumacząca napisy wyświetlane w oknie głównym
     */
    void retranslate(QMainWindow *main_window);

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy MainWindow
     */
    MainWindow(QWidget *parent = nullptr, unsigned int x = 1800, unsigned int y = 960);
    /*!
     * \brief Destruktor klasy MainWindow
     */
    ~MainWindow();
    /*!
     * \brief Ustawia widgety w prawej kolumnie okna aplikacji
     */
    void setVerticalLayout();
    /*!
     * \brief Ustawia widgety w poziomie (mapa + prawa kolumna)
     */
    void setHorizontalLayout();
    /*!
     * \brief Zwraca uchwyt do klasy modelującej port szeregowy
     *
     * Wskaźnik ten jest potrzebny w momencie uruchomienia wątku,
     * który go potrzebuje do odczytywania danych z pliku.
     *
     * \return wskaźnik do klasy SerialPort
     */
    SerialPort * returnSerial() { return serial; }
    /*!
     * \brief Funkcja przechwytująvca zdarzenia
     */
    virtual void changeEvent(QEvent *event) override;
    /*!
     * \brief Funkcja kontrolująca przepływ logiki zmiany języka
     */
    void languageChoice(int index);

   public slots:
    /*!
     * \brief Aktualizuje dane w każdym widgecie
     */
    void updateWindow();
    /*!
     * \brief Rozpoczyna pracę wątku
     */
    void startThread();
    /*!
     * \brief Zatrzymuje timer
     *
     * Konieczne do poprawnego zakończenia działania aplikacji.
     */
    void stopTimer() { timer->stop(); }
    /*!
     * \brief Rozpoczyna komunikację z portem szeregowym
     */
    void startSerialConnection();
    /*!
     * \brief Kończy komunikację z portem szeregowym
     */
    void endSerialConnection();
    /*!
     * \brief Rozpoczyna procedurę zmiany języka na polski
     */
    void plLangChosen();
    /*!
     * \brief Rozpoczyna procedurę zmiany języka na angielski
     */
    void engLangChosen();

  signals:
    /*!
     * \brief Port szeregowy gotowy do pracy
     */
    void serialReady();
    /*!
     * \brief Zakończenie połączenia z portem szeregowym
     */
    void serialEnded();
    /*!
     * \brief Błąd otworzenia portu szeregowego
     */
    void serialOpenError();
    /*!
     * \brief Komunikat o zmianie języka na polski
     */
    void typePl();
    /*!
     * \brief Komunikat o zmianie języka na angielski
     */
    void typeEng();
};

#endif // MAINWINDOW_HPP
