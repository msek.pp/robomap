QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport datavisualization serialport openglwidgets

CONFIG += c++11 qt opengl release

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/DistanceWin.cpp \
    src/FramedWidget.cpp \
    src/MainWindow.cpp \
    src/Map3D.cpp \
    src/NumericData.cpp \
    src/RobotData.cpp \
    src/RobotImage.cpp \
    src/RobotModel.cpp \
    src/SerialPort.cpp \
    src/Terminal.cpp \
    src/TravelInfo.cpp \
    src/VelocityGraph.cpp \
    src/Viewer.cpp \
    src/main.cpp \
    src/odbieranie.cpp \
    src/qcustomplot.cpp \
    src/transparam.cpp

HEADERS += \
    inc/DistanceWin.hpp \
    inc/FramedWidget.hpp \
    inc/MainWindow.hpp \
    inc/Map3D.hpp \
    inc/NumericData.hpp \
    inc/RobotData.hpp \
    inc/RobotImage.hpp \
    inc/RobotModel.hpp \
    inc/SerialPort.hpp \
    inc/Terminal.hpp \
    inc/TravelInfo.hpp \
    inc/VelocityGraph.hpp \
    inc/Viewer.hpp \
    inc/odbieranie.hh \
    inc/qcustomplot.hpp \
    inc/transparam.hh

FORMS += \
    ui/mainwindow.ui

TRANSLATIONS += Robomap_pl_PL.ts
TRANSLATIONS += Robomap_eng_ENG.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH+=inc /home/higgs_001/libQGLViewer-2.9.1
LIBS+=-L/home/higgs_001/libQGLViewer-2.9.1/QGLViewer -lQGLViewer-qt6

DISTFILES += \
    ui/RobotSumo.png
