#include "MainWindow.hpp"
#include <QStatusBar>

/*!
 * \file
 * \brief Definicje metod klasy MainWindow
 */

/*!
 * \brief Inicjalizuje obiekt klasy MainWindow
 *
 * Wywołuje konstruktor klasy bazowej QMainWindow, przekazując
 * jej wskaźnik na rodzica (nullptr). Tworzy dynamicznie
 * wszystkie widgety podrzędne kładąc je na atrybucie main_widget,
 * ustawia geometrię oraz łączy ze sobą odpowiednie sloty i sygnały.
 *
 * \param[in] parent - wskaźnik na rodzica (nullptr)
 * \param[in] x - szerokość okna
 * \param[in] y - wysokość okna
 */
MainWindow::MainWindow(QWidget *parent, unsigned int x, unsigned int y) : QMainWindow(parent)
{
// ~~~~~~~~~~~~~~~~~~~~~ UTWORZENIE WIDGET'ÓW ~~~~~~~~~~~~~~~~~~~ //

    // Inicjalizacja centralnego widget'u
    main_widget = new QWidget(this);
    setCentralWidget(main_widget);
    // Utworzenie menu
    createMenu();
    // Ustawienie paska statusowego
    setStatusBar(new QStatusBar());
    statusBar()->showMessage(tr("Aplikacja w trakcie działania..."));
    resize(x, y);
    // Inicjalizacja przycisków
    start_button = new QPushButton(tr("Rozpocznij"), main_widget);
    end_button = new QPushButton(tr("Zakończ"), main_widget);
    // Inicjalizacja terminala
    terminal = new Terminal("Terminal", main_widget);
    // Inicjalizacja dwóch wykresów prędkości kół
    graphs[0] = new VelocityGraph("Koło prawe", main_widget);
    graphs[1] = new VelocityGraph("Koło lewe", main_widget);
    // Inicjalizacja okna z przebytą drogą i koordynatami
    traveled_distance = new TravelInfo("Przebyta droga", main_widget);
    traveled_distance->writeLine("0.00");
    traveled_distance->setMaxWidth(60);
    coords = new TravelInfo("Współrzędne", main_widget);
    coords->writeLine("[ 0.00, 0.00 ]");
    coords->setMaxWidth(115);
    // Inicjalizacja mapy 3D
    map = new Map3D("Mapa 3D", main_widget);
    // Inicjalizacja okna z odległościami czujników
    distances = new DistanceWin("Odległości", main_widget);

    // Inicjalizacja portu szeregowego i wątku czytającego
    timer = new QTimer(this);
    serial = new SerialPort();

// ~~~~~~~~~~~~~~~~~~~~~ USTAWIENIE GEOMETRII ~~~~~~~~~~~~~~~~~~~ //

    // Dodanie zarządców geometrii do głównego widget'u
    travelinfo_layout = new QHBoxLayout();
    horizon_layout = new QHBoxLayout(main_widget);
    vertical_layout = new QVBoxLayout();
    setVerticalLayout();
    setHorizontalLayout();

// ~~~~~~~~~~~~~~~ USTAWIENIE SLOTÓW I SYGNAŁÓW ~~~~~~~~~~~~~~~~ //

    connect(start_button, SIGNAL(clicked()), this, SLOT(startSerialConnection()));
    connect(end_button, SIGNAL(clicked()), this, SLOT(endSerialConnection()));
    connect(end_button, SIGNAL(clicked()), this, SLOT(stopTimer()));
    connect(this, SIGNAL(serialEnded()), terminal, SLOT(typeSerialEnded()));
    connect(this, SIGNAL(serialReady()), terminal, SLOT(typeSerialReady()));
    connect(this, SIGNAL(serialReady()), this, SLOT(startThread()));
    connect(this, SIGNAL(serialOpenError()), terminal, SLOT(typeSerialOpenError()));
    connect(timer, SIGNAL(timeout()), this, SLOT(updateWindow()));

    connect(this, SIGNAL(typePl()), terminal, SLOT(typePlLang()));
    connect(this, SIGNAL(typeEng()), terminal, SLOT(typeEngLang()));
}
/*!
 * \brief Ustawia widgety w prawej kolumnie okna aplikacji
 *
 * Wcześniej ustawia widgety z informacjami o przemieszczeniu robota
 * w poziomie a następnie zarządca horyzontalny poddawany jest
 * zarządcy wertykalnemu (prawa kolumna okna aplikacji).
 */
void MainWindow::setVerticalLayout()
{
    travelinfo_layout->addWidget(traveled_distance);
    travelinfo_layout->addWidget(coords);

    vertical_layout->addWidget(distances);
    vertical_layout->addLayout(travelinfo_layout);
    vertical_layout->addWidget(graphs[1]);
    vertical_layout->addWidget(graphs[0]);
    vertical_layout->addWidget(terminal);
    vertical_layout->addWidget(start_button);
    vertical_layout->addWidget(end_button);
}
/*!
 * \brief Ustawia widgety w poziomie (mapa + prawa kolumna)
 *
 * Mapa oraz prawa kolumna oddzielone są sprężyną. Stosunek
 * ekspansji mapy i prawej kolumny to 3:1.
 */
void MainWindow::setHorizontalLayout()
{
    horizon_layout->addWidget(map);
    horizon_layout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::Minimum));
    horizon_layout->addLayout(vertical_layout);
    horizon_layout->setStretch(0, 3);
    horizon_layout->setStretch(2, 1);
}
/*!
 * \brief Aktualizuje dane w każdym widgecie
 *
 * Wywoływane są metody obiektów wyświetlanych w oknie
 * głównym, które powodują wyświetlenie nowych danych
 * odczytanych z portu szeregowego, w tym aktualizacja
 * mapy 3D.
 */
void MainWindow::updateWindow()
{
    // Uzyskanie wskaźnika do danych z robota
    RobotData *data = serial->returnRobotData();
    // Aktualizacja okienek z odległościami
    for(int i = 0; i < 4; ++i)
        distances->updateDistances(data->returnDistance(i), i);
    // Aktualizacja pozycji robota na mapie
    for(int i = 0; i < 4; ++i)
        map->passMeasure(data->returnDistance(i), i);

    map->moveRobot(data->returnVelocity(0), data->returnVelocity(1), TIME_INTERVAL);
    map->refresh();
    // Aktualizacja wykresów prędkości obu kół
    graphs[0]->updateWheelVelocity(data->returnVelocity(0), TIME_INTERVAL);
    graphs[1]->updateWheelVelocity(data->returnVelocity(1), TIME_INTERVAL);
    // Aktualizacja współrzędnych robota
    coords->countCoords(data->returnCounter(0), data->returnCounter(1));
    // Aktualizacja przebytej drogi robota
    traveled_distance->updateDistance(map->returnRobotDistance());
}

/*!
 * \brief Inicjalizuje menu aplikacji (wybór języka)
 *
 * Tworzy obiekty typu QAction, które wprowadzją opcje wyboru
 * języka w menu kontekstowym znajdującym się w lewym górnym
 * rogu okna głównego. Łączy sloty i sygnały odpowiadające za
 * rozpoczęcie procesu zmiany języka aplikacji.
 */
void MainWindow::createMenu()
{
    pl_lang = new QAction("&Polski", this);
    connect(pl_lang, &QAction::triggered, this, &MainWindow::plLangChosen);
    eng_lang = new QAction("&English", this);
    connect(eng_lang, &QAction::triggered, this, &MainWindow::engLangChosen);

    lang_menu = menuBar()->addMenu(tr("&Język"));
    lang_menu->addAction(pl_lang);
    lang_menu->addAction(eng_lang);
}
/*!
 * \brief Rozwija menu kontekstowe
 *
 * \param[in] event
 */
void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.addAction(pl_lang);
    menu.addAction(eng_lang);
    menu.exec(event->globalPos());
}
/*!
 * \brief Rozpoczyna pracę wątku
 *
 * Startuje pracę timer'a oraz budzi wątek przez
 * zmienną warunkową. Po tym wątek wpada w pętle
 * czytania danych z portu.
 */
void MainWindow::startThread()
{
    timer->start(TIME_INTERVAL);
    start.notify_one();
}
/*!
 * \brief Rozpoczyna komunikację z portem szeregowym
 *
 * Emituje sygnał o poprawnym otwarciu portu lub o błędym
 * otwarciu, co objawia się odpowiednim komunikatem w
 * terminalu.
 */
void MainWindow::startSerialConnection()
{
    if(serial->startConnection())
        emit serialReady();
    else
        emit serialOpenError();
}
/*!
 * \brief Kończy komunikację z portem szeregowym
 */
void MainWindow::endSerialConnection()
{
    if(serial->endConnection())
        emit serialEnded();
}
/*!
 * \brief Rozpoczyna procedurę zmiany języka na polski
 *
 * Emituje sygnał, który wyświetla stosowny komunikat
 * w terminalu.
 */
void MainWindow::plLangChosen()
{
    languageChoice(1);

    emit typePl();
}
/*!
 * \brief Rozpoczyna procedurę zmiany języka na angielski
 *
 * Emituje sygnał, który wyświetla stosowny komunikat
 * w terminalu.
 */
void MainWindow::engLangChosen()
{
    languageChoice(0);

    emit typeEng();
}
/*!
 * \brief Funkcja przechwytująvca zdarzenia
 *
 * Główne jej wykorzystanie w aplikacji, to przechwytywanie
 * zdarzenia związanego ze zmianą plików translacyjnych,co powoduje
 * tłumaczenie aplikacji.
 */
void MainWindow::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        retranslate(this);
        return;
    }
    QMainWindow::changeEvent(event);
}
/*!
 * \brief Metoda tłumacząca napisy wyświetlane w oknie głównym
 */
void MainWindow::retranslate(QMainWindow *main_window)
{
    main_window->setWindowTitle(QApplication::translate("MainWindow", "Application Window", nullptr));
    graphs[0]->changeTitle(QApplication::translate("MainWindow", "Left wheel", nullptr));
    graphs[1]->changeTitle(QApplication::translate("MainWindow", "Right wheel", nullptr));
    traveled_distance->changeTitle(QApplication::translate("MainWindow", "Traveled distance", nullptr));
    coords->changeTitle(QApplication::translate("MainWindow", "Coordinates", nullptr));
    distances->changeTitle(QApplication::translate("MainWindow", "Distancnes", nullptr));
    map->changeTitle(QApplication::translate("MainWindow", "3D map", nullptr));
    start_button->setText("Start");
    end_button->setText("End");
    statusBar()->showMessage("Application is running...");
}
/*!
 * \brief Funkcja kontrolująca przepływ logiki zmiany języka
 *
 * W przypadku indeksu równego 0, aplikacja tłumaczona jest na
 * angielski. Jeżeli 1, to tłumaczona na polski. Każde załadowanie
 * i usunięcie pliku translacyjnego powoduje pojawienie się zdarzenia
 * QEvent::LanguageChange, które z kolei powoduje wywołanie metody
 * 'retranslate()' i tłumaczenie aplikacji.
 */
void MainWindow::languageChoice(int index)
{
    static QTranslator *transPL = new QTranslator();

    switch (index)
    {
        case 0:
        {
            qApp->removeTranslator(transPL);
            QLocale Locale4EN(QLocale::English);
            break;
        }
        case 1:
        {
            if(transPL->load("Robomap_pl_PL.qm","../prj"))
            {
                qApp->installTranslator(transPL);
                QLocale Locale4PL(QLocale::Polish);
            }
            else
                std::cerr << "\nPlik 'Robomap_pl_PL.qm' nie został załadowany.";
            break;
        }
    }
}

/*!
 * \brief Destruktor klasy MainWindow
 */
MainWindow::~MainWindow() {}

