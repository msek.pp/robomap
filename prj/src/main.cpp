#include <QApplication>
#include "MainWindow.hpp"

#include <thread>
#include <condition_variable>

std::condition_variable start;
std::mutex cm;

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  MainWindow window;

  window.languageChoice(1);

  SerialPort *serial = window.returnSerial();

  window.show();

  std::thread reader(process, serial);

  int result = app.exec();

  reader.join();

  return result;
}
