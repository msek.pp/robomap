#include "TravelInfo.hpp"

/*!
 * \file
 * \brief Definicje metod klasy TravelInfo
 */

/*!
 * \brief Inicjalizuje obiekt klasy TravelInfo
 *
 * Wymusza wywołanie konstruktora klasy FramedWidget, przekazując
 * mu tytuł widgetu oraz wskaźnik na widget będący rodzicem. Tworzy
 * dynamicznie przechowywane atrybuty i wywołuje metody, które
 * przygotowywują widgety do wyświetlenia w oknie głównym.
 *
 * \param[in] widget_title - tytuł widgetu
 * \param[in] parent - wskaźnik na rodzica
 */
TravelInfo::TravelInfo(const char *widget_title, QWidget *parent) : FramedWidget(widget_title, parent)
{
    // Dynamiczne utworzenie atrybutów
    info = new QLineEdit(this);
    unit = new QLabel("m", this);
    hlayout = new QHBoxLayout();
    // Przygotowanie widgetów do wyświetlenia
    setStoredWidget();
    setHLayout();
    setVLayout();
    setMaxWidth();
    // Ustawienie własnej geometrii
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Konfiguruje atrybut info, nadając mu odpowiedni kolor, zachowanie
 * geometrii oraz tryb ReadOnly.
 */
void TravelInfo::setStoredWidget()
{
    info->setReadOnly(true);
    info->setFrame(true);
    info->setTextMargins(1, 1, 1, 1);
    info->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    info->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    info->setMaxLength(TRAVEL_INFO_LIMIT);
    info->setStyleSheet("color: white;");
}
/*!
 * \brief Ustawia widgety w poziomie
 *
 * Ustawia w poziomie edytor i jednostkę wyświetlanych danych
 */
void TravelInfo::setHLayout()
{
    hlayout->addWidget(info);
    hlayout->addWidget(unit);
}
/*!
 * \brief Ustawia widgety pochodne w pionie
 *
 * Idąc od góry, ustawiany jest tytuł a potem zarządca horyzontalny
 * przechowywujący edytor i jednostkę.
 */
void TravelInfo::setVLayout()
{
    vlayout->addWidget(title);
    vlayout->addLayout(hlayout);
}
/*!
 * \brief Liczy współrzędne robota na planszy
 *
 * Porównuje nowe pomiary ze starymi trzymanymi w atrybucie old_reads.
 * Jeżeli stan obydwu czujników zmienił się na przeciwny (co wiąże
 * się ze zmianą kratki z czarnej na białą lub odwrotnie), to
 * odnotowuje to jako zwiększenie współrzędnej osi X. Na koniec
 * wyświetla dane w oknie edytora.
 *
 * \param[in] left_opto - wartość napięcia z lewego transoptora
 * \param[in] right_opto - wartość napięcia z prawego transoptora
 */
void TravelInfo::countCoords(int left_opto, int right_opto)
{
    if(left_opto > old_reads[0] && right_opto < old_reads[1])
    {
        if((left_opto - old_reads[0]) > 300 && (old_reads[1] - right_opto) > 300)
            coords[0] += 1;
    }
    else if(left_opto < old_reads[0] && right_opto > old_reads[1])
    {
        if((old_reads[0] - left_opto) > 300 && (right_opto - old_reads[1]) > 300)
            coords[0] += 1;
    }

    old_reads[0] = left_opto;
    old_reads[1] = right_opto;

    updateCoords();
}
/*!
 * \brief Wpisuje linijkę do edytora
 *
 * \param[in] line - linijka tekstu umieszczana w edytorze
 */
void TravelInfo::writeLine(QString line)
{
    info->clear();
    info->insert(line);
}
/*!
 * \brief Konweruje przekazany dystans na metry i wyświetla w okienku
 *
 * Funkcja konwertuje odległość z [cm] na [m] i wpisuje dane do
 * edytora.
 *
 * \param[in] distance - odległość przebyta przez robota w [cm]
 */
void TravelInfo::updateDistance(double distance)
{
    double distance_m = distance / 100.0;
    QString printable = QString::number(distance_m, 'f', 3);
    // Czyści aktualną zawartość
    info->clear();
    // Wstawia odczyt razem z jednostką
    info->insert(printable + " m");
}
/*!
 * \brief Aktualizuje współrzędne robota w okienku
 *
 * Przekształca koordynaty z [cm] do [m] i wyświetla w edytorze.
 */
void TravelInfo::updateCoords()
{
    QString coord_x = QString::number((float)coords[0]/100.0, 'f', 3);
    QString coord_y = QString::number((float)coords[1]/100.0, 'f', 3);
    // Czyści aktualną zawartość
    info->clear();
    // Wstawia odczyt razem z jednostką
    info->insert("[ " + coord_x + ", " + coord_y + " ]");
}
