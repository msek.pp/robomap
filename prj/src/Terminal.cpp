#include "Terminal.hpp"

/*!
 * \file
 * \brief Definicje metod klasy Terminal
 */

/*!
 * \brief Inicjalizuje obiekt klasy Terminal
 *
 * Wymusza wywołanie konstruktora klasy FramedWidget, przekazując
 * mu tytuł widgetu oraz wskaźnik na widget będący rodzicem. Tworzy
 * dynamicznie przechowywane atrybuty i wywołuje metody, które
 * przygotowywują widgety do wyświetlenia w oknie głównym.
 *
 * \param[in] terminal_title - tytuł widgetu
 * \param[in] parent - wskaźnik na rodzica
 */
Terminal::Terminal(const char *terminal_title, QWidget *parent) : FramedWidget(terminal_title, parent)
{
    editor = new QTextEdit(this);
    // Definicja zachowania geometrii
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    setStoredWidget();
    setVLayout();
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Konfiguruje atrybut editor, nadając mu odpowiedni kolor, zachowanie
 * geometrii oraz tryb ReadOnly.
 */
void Terminal::setStoredWidget()
{
    editor->setReadOnly(true);
    editor->setStyleSheet("QTextEdit { background-color: rgb(125, 125, 125); }");
    editor->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
}
/*!
 * \brief Ustawia widgety pochodne w pionie
 *
 * Idąc od góry, ustawiany jest tytuł a następnie edytor.
 */
void Terminal::setVLayout()
{
    vlayout->addWidget(title);
    vlayout->addWidget(editor);
}
/*!
 * \brief Ustawia tło edytora
 *
 * \param palette - skonfigurowana paleta barw
 */
void Terminal::setBackgroundColour(const QPalette & palette)
{
    editor->setAutoFillBackground(true);
    editor->setPalette(palette);
}
/*!
 * \brief Zwraca aktualny czas w formie tekstu
 *
 * \return Zwraca stały obiekt typu QString zawierający datę w formacie
 *         hh:mm:ss.
 */
const QString Terminal::getTimeString()
{
    QDateTime date = QDateTime::currentDateTime();
    QString time = date.toString("hh:mm:ss");

    return time;
}
/*!
 * \brief Wyświetla komunikat o błędzie otworzenia portu szeregowego
 */
void Terminal::typeSerialOpenError()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("Nie można połączyć z portem szeregowym") +
                    ".\n");
}
/*!
 * \brief Wyświetla komunikat o błędzie wczytania danych
 */
void Terminal::typeSerialReadError()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("Nieprawidłowe odczytanie danych z portu szeregowego") +
                    ".\n");
}
/*!
 * \brief Wyświetla komunikat o zakończeniu połączenia z portem
 */
void Terminal::typeSerialEnded()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("Zakończenie połączenia z portem szeregowym") +
                    ".\n");
}
/*!
 * \brief Wyświetla komunikat o prawidłowej konfiguracji portu
 */
void Terminal::typeSerialReady()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("Połączono z portem szeregowym") +
                    ".\n");
}
/*!
 * \brief Wyświetla komunikat o wykryciu przeszkody
 */
void Terminal::typeObstacleDetected()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("Wykryto przeszkodę") +
                    "!\n");
}
/*!
 * \brief Wyświetla komunikat o zmianie języka na polski
 */
void Terminal::typePlLang()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("Wybrano język polski") +
                    "\n");
}
/*!
 * \brief Wyświetla komunikat o zmianie języka na angielski
 */
void Terminal::typeEngLang()
{
    editor->append("~> " + getTimeString() + ": " +
                    tr("English has been chosen") +
                    "\n");
}
