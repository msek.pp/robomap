#include "RobotModel.hpp"

/*!
 * \file
 * \brief Definicje metod klasy RobotModel
 */

/*!
 * \brief Oblicza nowe położenie i orietnację robota
 *
 * Na podstawie wypełnienia sygnału PWM obu kół oraz danego
 * odcinka czasu obliczane są nowe położenie na mapie oraz orientacja.
 * Wypełnienie przeliczane jest na prędkość liniową i obrotową
 * punktu między kołami robota, w jednostkach kolejno [cm/s] i
 * [rad/s]. Mnożąc te prędkości przez dany czas, otrzymuje się
 * przemieszczenie, które to przeliczane jest na współrzędne x i
 * y.
 *
 * \param[in] left_velocity - wypełnienie PWM lewego koła
 * \param[in] right_velocity - wypełnienie PWM prawego koła
 * \param[in] time_interval - odcinek czasu, w którym robot się
 *            przemieszczał z tymi prędkościami (czas odliczany przez
 *            główny timer ~ 50ms).
 */
void RobotModel::move(int left_velocity, int right_velocity, int time_interval)
{
    // Przerobienie prędkości z wypełnienia PWM na cm/s
    double left, right;

    if(left_velocity < 0)
        left = -((-left_velocity) - OFFSET)/LINEAR_COEF;
    else
        left = (left_velocity == 0 ? 0 : (left_velocity - OFFSET)/LINEAR_COEF);
    if(right_velocity < 0)
        right = -((-right_velocity) - OFFSET)/LINEAR_COEF;
    else
        right = (right_velocity == 0 ? 0 : (right_velocity - OFFSET)/LINEAR_COEF);

    // Przesunięcie kątowe kół zależne od prędkości robota
    lwheel_rot += left / WHEEL_RADIUS_EXT;
    rwheel_rot -= right / WHEEL_RADIUS_EXT;
    if(lwheel_rot > 360)
        lwheel_rot = 0;
    if(rwheel_rot < -360)
        rwheel_rot = 0;

    // Przeliczenie prędkości liniowej i kątowej środka robota
    double velocity = (1.0/2.0 * left) + (1.0/2.0 * right);
    double omega = (1.0/WHEEL_SPACING)*right - (1.0/WHEEL_SPACING)*left;
    omega = rad2deg(omega); // Prędkość kątowa w stopniach na sekundę

    // Liczenie przesunięć
    double linear_move = velocity * time_interval/1000;
    double angle_move = omega * time_interval/1000;
    orientation += angle_move;
    distance += linear_move;

    // Nadpisanie danych w klasie do rysowania
    position[0] += -cos(deg2rad(orientation)) * linear_move;
    position[1] += -sin(deg2rad(orientation)) * linear_move;
}

/*!
 * \brief Oblicza współrzędne przeszkód
 *
 * Jeżeli jest wystarczająca ilość pomiarów w buforze, funkcja
 * rozpoczyna obliczanie współrzędnych przeszkód. Pod warunkiem, że
 * pomiar z danego czujnika jest stały, obliczane są współrzędne
 * względem punktu między kołami robota znajdującego się w punkcie
 * (0, 0). Sprawdzane jest czy wykryte przeszkody mieszczą się w
 * kręgu detekcji robota. Jeżeli tak, to są one dodatkowo przesuwane
 * we wcześniej obliczone miejsce pobytu robota na mapie, odpowiednio
 * obracane i dodawane do wektora, z któ©ego potem są rysowane.
 */
void RobotModel::drawObstacle()
{
    if(!checkMeasuresSize())
        return;

    float coords[2];
    float measure;
    // Wizualizacja danych z lewego czujnika
    measure = returnMeasureIfConst(0);
    if(measure != -1)
    {
        float left_measure = measure / 10.0; // Z mm na cm
        coords[0] = (sin(deg2rad(180 + ANGLE)) * left_measure) - OFFSETX1;
        coords[1] = (cos(deg2rad(180 + ANGLE)) * left_measure) - OFFSETY1;
        if(pow(coords[0], 2) + pow(coords[1], 2) < pow(DETECTION_RADIUS, 2))
        {
            coords[0] = coords[0]*cos(deg2rad(orientation)) - coords[1]*sin(deg2rad(orientation));
            coords[1] = coords[1]*cos(deg2rad(orientation)) + coords[0]*sin(deg2rad(orientation));
            obstacles.push_back(std::make_pair(coords[0] + position[0], coords[1] + position[1]));
        }
    }
    // Wizualizacja danych pierwszego przedniego czujnika
    measure = returnMeasureIfConst(1);
    if(measure != -1)
    {
        float front1_measure = measure / 10.0;
        coords[0] = -(front1_measure + OFFSETX2);
        coords[1] = -OFFSETY2;
        if(pow(coords[0], 2) + pow(coords[1], 2) < pow(DETECTION_RADIUS, 2))
        {
            coords[0] = coords[0]*cos(deg2rad(orientation)) - coords[1]*sin(deg2rad(orientation));
            coords[1] = coords[1]*cos(deg2rad(orientation)) + coords[0]*sin(deg2rad(orientation));
            obstacles.push_back(std::make_pair(coords[0] + position[0], coords[1] + position[1]));
        }
    }
    // Wizualizacja danych drugiego przedniego czujnika
    measure = returnMeasureIfConst(2);
    if(measure != -1)
    {
        float front2_measure = measure / 10.0;
        coords[0] = -(front2_measure + OFFSETX2);
        coords[1] = OFFSETY2;
        if(pow(coords[0], 2) + pow(coords[1], 2) < pow(DETECTION_RADIUS, 2))
        {
            coords[0] = coords[0]*cos(deg2rad(orientation)) - coords[1]*sin(deg2rad(orientation));
            coords[1] = coords[1]*cos(deg2rad(orientation)) + coords[0]*sin(deg2rad(orientation));
            obstacles.push_back(std::make_pair(coords[0] + position[0], coords[1] + position[1]));
        }
    }
    // Wizualizacja danych z prawego czujnika
    measure = returnMeasureIfConst(3);
    if(measure != -1)
    {
        float right_measure = measure / 10.0; // Z mm na cm
        coords[0] = (sin(deg2rad(-ANGLE)) * right_measure) - OFFSETX1 + position[0];
        coords[1] = (cos(deg2rad(-ANGLE)) * right_measure) + OFFSETY1 + position[1];
        if(pow(coords[0], 2) + pow(coords[1], 2) < pow(DETECTION_RADIUS, 2))
        {
            coords[0] = coords[0]*cos(deg2rad(orientation)) - coords[1]*sin(deg2rad(orientation));
            coords[1] = coords[1]*cos(deg2rad(orientation)) + coords[0]*sin(deg2rad(orientation));
            obstacles.push_back(std::make_pair(coords[0] + position[0], coords[1] + position[1]));
        }
    }

    for(unsigned int i = 0; i < obstacles.size(); ++i)
        drawObstacleLine(obstacles[i], obstacle_color);

    if(obstacles.size() >= MAX_BUF_SIZE)
        obstacles.erase(obstacles.begin(), obstacles.begin() + 1000);
}
/*!
 * \brief Sprawdza czy zebrano wystarczającą ilość pomiarów
 *
 * Wymagane jest przynajmniej 8 pomiarów odległości w tablicy,
 * aby móc sprawdzić czy pomiar jest stabilny i żeby można było
 * go uśrednić.
 */
bool RobotModel::checkMeasuresSize()
{
    bool can_draw = true;
    for(int i = 0; i < 4; ++i)
    {
        if(measures[i].size() < 8)
        {
            can_draw = false;
            break;
        }
    }

    return can_draw;
}
/*!
 * \brief Zwraca uśredniony wynik, o ile ostatnie pomiary były stabilne
 *
 * Sprawdza stabilność ostatnich 8 pomiarów odległości w tablicy.
 * Jeżeli oscylują w granicy +/- 7mm, to są one uśredniane i
 * przekazywane jako jeden pomiar do funkcji liczącej współrzędne
 * przeszkód. W ten sposób ogranicza się rysowanie na mapie niezidentyfikowanych
 * punktów pochodzących z błędów sensorów czy punktów pochodzących
 * ze zbliżania się robota do przeszkody (efekt "promieni").
 */
int RobotModel::returnMeasureIfConst(int index)
{
    bool count_mean = true;
    int size = measures[index].size();
    int result = -1;

    for(int i = size - 1; i > size - 8; --i)
    {
        int diff = measures[index][i] - measures[index][i - 1];
        if(!((diff < 0 && diff >= -7) || (diff > 0 && diff <= 7)))
        {
            count_mean = false;
            break;
        }
    }

    if(count_mean)
    {
        int mean = 0;
        for(int i = size - 1; i >= size - 8; --i)
            mean += measures[index][i];

        result = mean / 8;
    }

    if(size >= 16)
        measures[index].erase(measures[index].begin(), measures[index].begin() + 8);

    return result;
}
/*!
 * \brief Rysuje wszystkie elementy mapy 3D
 *
 * Rysuje robota zorientowanego wzdłuż dodatniej osi X a
 * następnie jego krąg detekcji. Jest on przesuwany wzdłuż
 * osi X o 1.55 cm, ponieważ wtedy środek układu wsp. znajduje
 * się w środku pomiędzy jego kołami (model kinematyki tego wymaga).
 * Robot z półokręgiem obracany jest o 180 stopnii i przesuwany na obliczoną
 * pozycję (na początku jest to środek początku planszy). Na koniec
 * odrysowywane są przeszkody z bufora oraz sama plansza.
 */
void RobotModel::build()
{
    drawBoard();
    // Narysowanie linii przeszkód
    drawObstacle();
    // Przesunięcie robota na odpowiednie miejsce na planszy
    glTranslatef(position[0], position[1], 0);
    // Obrót robota o odpowiedni kąt
    glRotatef(orientation, 0, 0, 1);
    // Przesunięcie robota do punktu środkowego między kołami (do modelu kinematycznego)
    glTranslatef(1.55, 0, 0);
    // Narysowanie okręgu detekcji przeszkód
    drawDetectionCircle();
    // Zbudowanie robota
    buildRobot();
}
/*!
 * \brief Rysuje kompletnego robota
 *
 * Składa kompletenego robota, zaczynając od narysowania silników, potem
 * baterii, podstawy, kół a na koniec przedniej ściany.
 */
void RobotModel::buildRobot()
{
    glPushMatrix();
    glTranslatef(-8.4, 0, 0.9);
    glRotatef(180, 0, 0, 1);
    drawFront();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-1.55, 3.4 + WHEEL_WIDTH, WHEEL_RADIUS_EXT + 0.5);
    glRotatef(180, 0, 0, 1);
    drawWheel(rwheel_rot);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-1.55, -3.4 - WHEEL_WIDTH, WHEEL_RADIUS_EXT + 0.5);
    drawWheel(lwheel_rot);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 0, 1.3);
    glRotatef(180, 0, 0, 1);
    glRotatef(4.494, 0, 1, 0);
    drawBase();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-4.2, -3.25, 1);
    glRotatef(-4.494, 0, 1, 0);
    drawBattery();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-1.55, 3.4, 2.0);
    glRotatef(180, 0, 0, 1);
    glRotatef(4.494, 0, 1, 0);
    drawMotor();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-1.55, -3.4, 2.0);
    glRotatef(-4.494, 0, 1, 0);
    drawMotor();
    glPopMatrix();
}
/*!
 * \brief Rysuje kompletną przednią ścianę robota
 *
 * Zaczyna od narysowania dwóch bocznych pięciokątów zorientowanych do wewnątrz
 * ściany o kąt wewnętrzny 155 stopnii. Na koniec rysuje samą frontową część.
 */
void RobotModel::drawFront()
{
    glRotatef(-10, 0, 1, 0);

    drawFrontWall();

    glPushMatrix();
    glTranslatef(0, -2.34, 0);
    glRotatef(155, 0, 0, 1);
    glRotatef(1.8, 0, 1, 0);
    glTranslatef(-0.2, 0, 0);
    drawFrontWing();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 2.347, 0);
    glRotatef(25, 0, 0, 1);
    glRotatef(-1.8, 0, 1, 0);
    drawFrontWing();
    glPopMatrix();
}
/*!
 * \brief Rysuje frontową część ściany
 *
 * Zwykły prostopadłościan o szerokości 2mm zorientowany w pionie.
 */
void RobotModel::drawFrontWall()
{
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 2.347, 0);
        glVertex3f(0, 1.978, 5.488);
        glVertex3f(0, -1.978, 5.488);
        glVertex3f(0, -2.347, 0);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0.2, 2.347, 0);
        glVertex3f(0.2, 1.978, 5.488);
        glVertex3f(0.2, -1.978, 5.488);
        glVertex3f(0.2, -2.347, 0);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 2.347, 0);
        glVertex3f(0.2, 2.347, 0);
        glVertex3f(0.2, -2.347, 0);
        glVertex3f(0, -2.347, 0);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 2.347, 0);
        glVertex3f(0.2, 2.347, 0);
        glVertex3f(0.2, 1.978, 5.488);
        glVertex3f(0, 1.978, 5.488);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 1.978, 5.488);
        glVertex3f(0.2, 1.978, 5.488);
        glVertex3f(0.2, -1.978, 5.488);
        glVertex3f(0, -1.978, 5.488);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, -2.347, 0);
        glVertex3f(0.2, -2.347, 0);
        glVertex3f(0.2, -1.978, 5.488);
        glVertex3f(0, -1.978, 5.488);
    glEnd();
}
/*!
 * \brief Rysuje lewą i prawą część ściany
 *
 * Odchylone do wewnątrz pięciokąty łączące się z prostokątną
 * ścianą najdłuższymi bokami. We właściwej funkcji budującej
 * frontową ścianę, są one odchylane do wewnątrz pod kątem
 * 155 stopnii.
 */
void RobotModel::drawFrontWing()
{
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 2.821, 0.38);
        glVertex3f(0, 2.826, 1.39);
        glVertex3f(0, 0.622, 5.626);
        glVertex3f(0, -0.368, 5.488);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0.2, 0, 0);
        glVertex3f(0.2, 2.821, 0.38);
        glVertex3f(0.2, 2.826, 1.39);
        glVertex3f(0.2, 0.622, 5.626);
        glVertex3f(0.2, -0.368, 5.488);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 2.821, 0.38);
        glVertex3f(0.2, 2.821, 0.38);
        glVertex3f(0.2, 0, 0);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 2.821, 0.38);
        glVertex3f(0, 2.826, 1.39);
        glVertex3f(0.2, 2.826, 1.39);
        glVertex3f(0.2, 2.821, 0.38);
    glEnd();

    glBegin(GL_POLYGON);
    glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 2.826, 1.39);
        glVertex3f(0, 0.622, 5.626);
        glVertex3f(0.2, 0.622, 5.626);
        glVertex3f(0.2, 2.826, 1.39);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 0.622, 5.626);
        glVertex3f(0, -0.368, 5.488);
        glVertex3f(0.2, -0.368, 5.488);
        glVertex3f(0.2, 0.622, 5.626);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, -0.368, 5.488);
        glVertex3f(0, 0, 0);
        glVertex3f(0.2, 0, 0);
        glVertex3f(0.2, -0.368, 5.488);
    glEnd();
}
/*!
 * \brief Rysuje podstawę robota
 *
 * Ze względu na dosyć skomplikowaną konstrukcję podstawy, wykorzystano
 * funkcję rysującą odpowiednie wielokąty przy danych współrzędnych (nieco
 * uprościło wygląd samej funkcji).
 */
void RobotModel::drawBase()
{
    float x_coords[4];
    float y_coords[4];
    float z_coords[4] = {0, 0, 0.3, 0.3};
    float y_normal[3] = {0, 1, 0};
    // ~~~~~~~~ Spód podstawy robota ~~~~~~~ //
    updateArray(x_coords, base_xdim[0], base_xdim[1], base_xdim[1], base_xdim[0]);
    updateArray(y_coords, base_ydim[0], base_ydim[0], -base_ydim[0], -base_ydim[0]);
    // Do zmiany później!
    updateArray(z_coords, 0.3, 0.3, 0.3, 0.3);

    drawPolygon(x_coords, y_coords, z_coords, base_color);
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(base_color);
        glVertex3f(base_xdim[1], base_ydim[1], 0);
        glVertex3f(base_xdim[2], base_ydim[1], 0);
        glVertex3f(base_xdim[3], base_ydim[2], 0);
        // Odbicie lustrzane
        glVertex3f(base_xdim[3], -base_ydim[2], 0);
        glVertex3f(base_xdim[2], -base_ydim[1], 0);
        glVertex3f(base_xdim[1], -base_ydim[1], 0);
    glEnd();
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~ Góra podstawy robota ~~~~~~~ //
    updateArray(x_coords, base_xdim[0], base_xdim[1], base_xdim[1], base_xdim[0]);
    updateArray(y_coords, base_ydim[0], base_ydim[0], -base_ydim[0], -base_ydim[0]);
    drawPolygon(x_coords, y_coords, z_coords, base_color);
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(base_color);
        glVertex3f(base_xdim[1], base_ydim[1], 0.3);
        glVertex3f(base_xdim[2] - 1.5, base_ydim[1], 0.3);
        glVertex3f(base_xdim[3] - 1.5, base_ydim[2] - 0.7 , 0.3);
        // Odbicie lustrzane
        glVertex3f(base_xdim[3] - 1.5, -base_ydim[2] + 0.7, 0.3);
        glVertex3f(base_xdim[2] -1.5, -base_ydim[1], 0.3);
        glVertex3f(base_xdim[1], -base_ydim[1], 0.3);
    glEnd();
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~ Skosy podstawy ~~~~~~~~~~~ //
    updateArray(z_coords, 0, 0, 0.3, 0.3);
    // Skos lewy
    updateArray(x_coords, base_xdim[2], base_xdim[3], base_xdim[3] - 1.5, base_xdim[2] - 1.5);
    updateArray(y_coords, base_ydim[1], base_ydim[2], base_ydim[2] - 0.7, base_ydim[1]);
    drawPolygon(x_coords, y_coords, z_coords, base_color);
    // Skos prawy
    updateArray(x_coords, base_xdim[2], base_xdim[3], base_xdim[3] - 1.5, base_xdim[2] - 1.5);
    updateArray(y_coords, -base_ydim[1], -base_ydim[2], -base_ydim[2] + 0.7, -base_ydim[1]);
    drawPolygon(x_coords, y_coords, z_coords, base_color);
    // Skos przedni
    updateArray(x_coords, base_xdim[3], base_xdim[3], base_xdim[3] - 1.5, base_xdim[3] - 1.5);
    updateArray(y_coords, -base_ydim[2], base_ydim[2], base_ydim[2] - 0.7, -base_ydim[2] + 0.7);
    drawPolygon(x_coords, y_coords, z_coords, base_color);
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~ Boki podstawy ~~~~~~~~~~~ //
    // Boki podstawy robota wzdłuż osi Y
    updateArray(x_coords, 0, 0, 0, 0);
    updateArray(y_coords, -3.4, 3.4, 3.4, -3.4);
    drawPolygon(x_coords, y_coords, z_coords, base_color, y_normal);

    updateArray(x_coords, 3.1, 3.1, 3.1, 3.1);
    updateArray(y_coords, -3.4, -4.9, -4.9, -3.4);
    drawPolygon(x_coords, y_coords, z_coords, base_color, y_normal);

    updateArray(x_coords, 3.1, 3.1, 3.1, 3.1);
    updateArray(y_coords, 3.4, 4.9, 4.9, 3.4);
    drawPolygon(x_coords, y_coords, z_coords, base_color, y_normal);
    // Boki podstawy robota wzdłuż osi X
    updateArray(x_coords, 0, 3.1, 3.1, 0);
    updateArray(y_coords, -3.4, -3.4, -3.4, -3.4);
    drawPolygon(x_coords, y_coords, z_coords, base_color);

    updateArray(x_coords, 0, 3.1, 3.1, 0);
    updateArray(y_coords, 3.4, 3.4, 3.4, 3.4);
    drawPolygon(x_coords, y_coords, z_coords, base_color);

    updateArray(x_coords, 3.1, 8.4, 6.9, 3.1);
    updateArray(y_coords, -4.9, -4.9, -4.9, -4.9);
    drawPolygon(x_coords, y_coords, z_coords, base_color);

    updateArray(x_coords, 3.1, 8.4, 6.9, 3.1);
    updateArray(y_coords, 4.9, 4.9, 4.9, 4.9);
    drawPolygon(x_coords, y_coords, z_coords, base_color);
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

}
/*!
 * \brief Rysuje baterię
 *
 * Zwykły prostopadłościan wymiarami zbliżony do
 * prawdziwej baterii.
 */
void RobotModel::drawBattery()
{
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(-1.15, 0, 0);
        glVertex3f(1.15, 0, 0);
        glVertex3f(1.15, 6.5, 0);
        glVertex3f(-1.15, 6.5, 0);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(-1.15, 0, 1.2);
        glVertex3f(1.15, 0, 1.2);
        glVertex3f(1.15, 6.5, 1.2);
        glVertex3f(-1.15, 6.5, 1.2);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(-1.15, 0, 0);
        glVertex3f(-1.15, 6.5, 0);
        glVertex3f(-1.15, 6.5, 1.2);
        glVertex3f(-1.15, 0, 1.2);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(1.15, 0, 0);
        glVertex3f(1.15, 6.5, 0);
        glVertex3f(1.15, 6.5, 1.2);
        glVertex3f(1.15, 0, 1.2);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(-1.15, 0, 0);
        glVertex3f(1.15, 0, 0);
        glVertex3f(1.15, 0, 1.2);
        glVertex3f(-1.15, 0, 1.2);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(-1.15, 6.5, 0);
        glVertex3f(1.15, 6.5, 0);
        glVertex3f(1.15, 6.5, 1.2);
        glVertex3f(-1.15, 6.5, 1.2);
    glEnd();
}
/*!
 * \brief Rysuje koło robota
 *
 * \param[in] rotation - kąt, od którego rozpoczynane jest rysowanie koła
 *                       (animacja obrotu koła)
 */
void RobotModel::drawWheel(double rotation)
{
    float x_position;
    float z_position;
    float sinus = 0;
    float cosinus = 1;

    // Rysowanie opony / bieżnika
    glBegin(GL_QUAD_STRIP);
        glColor3f(0.3, 0.3, 0.3);

        for(double ang_deg = rotation; ang_deg <= 360 + rotation; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = WHEEL_RADIUS_EXT * cosinus;
            z_position = WHEEL_RADIUS_EXT * sinus;

            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, WHEEL_WIDTH, z_position);
        }
    glEnd();
    // Rysowanie lewego boku koła
    glBegin(GL_QUAD_STRIP);
        glColor3f(0.3, 0.3, 0.3);

        for(double ang_deg = rotation; ang_deg <= 360 + rotation; ang_deg += ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = WHEEL_RADIUS_IN * cosinus;
            z_position = WHEEL_RADIUS_IN * sinus;
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
            x_position = WHEEL_RADIUS_EXT * cosinus;
            z_position = WHEEL_RADIUS_EXT * sinus;
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
        }
    glEnd();
    // Ścianki wewnętrznego wycięcia w kole
    glBegin(GL_QUAD_STRIP);
        glColor3f(0, 0, 0);

        for(double ang_deg = rotation; ang_deg <= 360 + rotation; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = WHEEL_RADIUS_IN * cosinus;
            z_position = WHEEL_RADIUS_IN * sinus;

            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0.9, z_position);
        }
    glEnd();
    // Zamknięcie wewnętrznego wycięcia w kole
    glTranslatef(0, 0.9, 0);
    glBegin(GL_TRIANGLE_STRIP);
        glColor3f(0, 0, 0);

        for(double ang_deg = rotation; ang_deg <= 360 + rotation; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = WHEEL_RADIUS_IN * cosinus;
            z_position = WHEEL_RADIUS_IN * sinus;

            glVertex3f(0, 0, 0);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
        }
    glEnd();
    // Zamknięcie tylne koła
    glTranslatef(0, WHEEL_WIDTH - 0.9, 0);
    glBegin(GL_TRIANGLE_STRIP);
        glColor3f(0.3, 0.3, 0.3);

        for(double ang_deg = rotation; ang_deg <= 360 + rotation; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = WHEEL_RADIUS_EXT * cosinus;
            z_position = WHEEL_RADIUS_EXT * sinus;

            glVertex3f(0, 0, 0);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
        }
    glEnd();
}
/*!
 * \brief Rysuje okrąg detekcji robota
 *
 * Właściwie półokrąg, obejmujący wszystkie cztery czujniki.
 * Wszystkie przeszkody, które znajdą się w jego wnętrzu, są
 * rysowane.
 */
void RobotModel::drawDetectionCircle()
{
    float x_position;
    float y_position;
    float sinus = 0;
    float cosinus = 1;

    if(position[0])

    // Rysowanie opony / bieżnika
    glBegin(GL_QUAD_STRIP);
        glColor3f(0.9, 0.1, 0.1);

        for(double ang_deg = 180; ang_deg <= 360; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = DETECTION_RADIUS * sinus;
            y_position = DETECTION_RADIUS * cosinus;

            glNormal3f(x_position, y_position, 0.5);
            glVertex3f(x_position, y_position, 0.5);
            glNormal3f(x_position, y_position, 0.5);
            glVertex3f(x_position, y_position, 0.6);
        }
    glEnd();
}
/*!
 * \brief Rysuje kompletny silnik DC
 *
 * Silnik składa się z dwóch złotych blaszek, dwóch poprzeczek,
 * które te blaszki łączą oraz komory silnika
 */
void RobotModel::drawMotor()
{
    glPushMatrix();
    glTranslatef(0, 0.9, 0);
    drawMotorMain();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.4, 0.06, 0.2);
    drawMotorRoller();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-0.4, 0.06, -0.2);
    drawMotorRoller();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 0.84, 0);
    drawMotorFront();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(0, 0.46, 0);
    drawMotorFront();
    glPopMatrix();
    drawMotorFront();
}
/*!
 * \brief Rysuje główną część silnika
 *
 * Część ta składa się z dwóch prostokątów równoległych do siebie,
 * których dłuższe boki połączone są półokrągłymi powierzchniami
 * (zasadniczo paskiem prostokątów będących względem siebie pod pewnym kątem).
 * Komora potem łączona jest z blaszkami i tulejami.
 */
void RobotModel::drawMotorMain()
{
    float x_position;
    float z_position;
    float sinus = 0;
    float cosinus = 1;

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(-0.35, 0, -0.5);
        glVertex3f(0.35, 0, -0.5);
        glVertex3f(0.35, 1.5, -0.5);
        glVertex3f(-0.35, 1.5, -0.5);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(-0.35, 0, 0.5);
        glVertex3f(0.35, 0, 0.5);
        glVertex3f(0.35, 1.5, 0.5);
        glVertex3f(-0.35, 1.5, 0.5);
    glEnd();

    glBegin(GL_QUAD_STRIP);
        glColor3fv(front_color);

        for(double ang_deg = -56.44; ang_deg <= 56.44; ang_deg += 2)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.6 * cosinus;
            z_position = 0.6 * sinus;

            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 1.5, z_position);
        }
    glEnd();

    glBegin(GL_QUAD_STRIP);
        glColor3fv(front_color);

        for(double ang_deg = -56.44; ang_deg <= 56.44; ang_deg += 2)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.6 * cosinus;
            z_position = 0.6 * sinus;

            glNormal3f(-x_position, 0, z_position);
            glVertex3f(-x_position, 0, z_position);
            glNormal3f(-x_position, 0, z_position);
            glVertex3f(-x_position, 1.5, z_position);
        }
    glEnd();

    glPushMatrix();
    glTranslatef(0, 1.49, 0);
    glBegin(GL_TRIANGLE_STRIP);
        glColor3fv(front_color);

        for(double ang_deg = -56.44; ang_deg <= 56.44; ang_deg += 2)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.6 * cosinus;
            z_position = 0.6 * sinus;

            glVertex3f(0, 0, 0);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
        }
    glEnd();

    glBegin(GL_TRIANGLE_STRIP);
        glColor3fv(front_color);

        for(double ang_deg = -56.44; ang_deg <= 56.44; ang_deg += 2)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.6 * cosinus;
            z_position = 0.6 * sinus;

            glVertex3f(0, 0, 0);
            glNormal3f(-x_position, 0, z_position);
            glVertex3f(-x_position, 0, z_position);
        }
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 0, 0);
        glVertex3f(0.35, 0, 0.4873);
        glVertex3f(-0.35, 0, 0.4873);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3fv(front_color);
        glVertex3f(0, 0, 0);
        glVertex3f(0.35, 0, -0.4873);
        glVertex3f(-0.35, 0, -0.4873);
    glEnd();
    glPopMatrix();
}
/*!
 * \brief Rysuje cienkie tuleje silnika
 *
 * Te tuleje łączą ze sobą blaszki silnika.
 */
void RobotModel::drawMotorRoller()
{
    float x_position;
    float z_position;
    float sinus = 0;
    float cosinus = 1;

    glBegin(GL_QUAD_STRIP);
        glColor3f(255, 215, 0);

        for(double ang_deg = 0; ang_deg <= 360; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.175 * cosinus;
            z_position = 0.175 * sinus;

            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0, z_position);
            glNormal3f(x_position, 0, z_position);
            glVertex3f(x_position, 0.78, z_position);
        }
    glEnd();
}
/*!
 * \brief Rysuje złote blaszki silnika DC
 *
 * Blaszki są zorientowanymi pionowo prostopadłościanami
 * o grubości 0.6mm
 */
void RobotModel::drawMotorFront()
{
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(255, 215, 0);
        glVertex3f(-0.6, 0, -0.5);
        glVertex3f(0.6, 0, -0.5);
        glVertex3f(0.6, 0, 0.5);
        glVertex3f(-0.6, 0, 0.5);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(255, 215, 0);
        glVertex3f(-0.6, 0.06, -0.5);
        glVertex3f(0.6, 0.06, -0.5);
        glVertex3f(0.6, 0.06, 0.5);
        glVertex3f(-0.6, 0.06, 0.5);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(255, 215, 0);
        glVertex3f(-0.6, 0, -0.5);
        glVertex3f(-0.6, 0.06, -0.5);
        glVertex3f(0.6, 0.06, -0.5);
        glVertex3f(0.6, 0, -0.5);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(255, 215, 0);
        glVertex3f(0.6, 0, -0.5);
        glVertex3f(0.6, 0.06, -0.5);
        glVertex3f(0.6, 0.06, 0.5);
        glVertex3f(0.6, 0, 0.5);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(255, 215, 0);
        glVertex3f(0.6, 0, 0.5);
        glVertex3f(0.6, 0.06, 0.5);
        glVertex3f(-0.6, 0.06, 0.5);
        glVertex3f(-0.6, 0, 0.5);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(255, 215, 0);
        glVertex3f(-0.6, 0, 0.5);
        glVertex3f(-0.6, 0.06, 0.5);
        glVertex3f(-0.6, 0.06, -0.5);
        glVertex3f(-0.6, 0, -0.5);
    glEnd();
}
/*!
 * \brief Rysuje planszę, po której jeździ robot
 *
 * Po narysowaniu samego białego prostopadłościanu o rozmiarach
 * 60x42x0.5 [cm], rysowane są czarne pola planszy tworząc w ten
 * sposób szachownicę - odzwierciedlenie rzeczywistej planszy, po
 * której porusza się robot.
 */
void RobotModel::drawBoard()
{
    // Rysowanie baiłego prostopadłościanu będącego planszą
    // Dół
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(1, 1, 1);
        glVertex3f((BOARD_X/2), -(BOARD_Y/2), 0);
        glVertex3f(-(BOARD_X/2), -(BOARD_Y/2), 0);
        glVertex3f(-(BOARD_X/2), (BOARD_Y/2), 0);
        glVertex3f((BOARD_X/2), (BOARD_Y/2), 0);
    glEnd();
    // Góra
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(1, 1, 1);
        glVertex3f((BOARD_X/2), -(BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), -(BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), (BOARD_Y/2), 0.5);
        glVertex3f((BOARD_X/2), (BOARD_Y/2), 0.5);
    glEnd();
    // Lewy bok
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(1, 1, 1);
        glVertex3f((BOARD_X/2), -(BOARD_Y/2), 0);
        glVertex3f((BOARD_X/2), -(BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), -(BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), -(BOARD_Y/2), 0);
    glEnd();
    // Prawy bok
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(1, 1, 1);
        glVertex3f((BOARD_X/2), (BOARD_Y/2), 0);
        glVertex3f((BOARD_X/2), (BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), (BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), (BOARD_Y/2), 0);
    glEnd();
    // Przód
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(1, 1, 1);
        glVertex3f((BOARD_X/2), -(BOARD_Y/2), 0);
        glVertex3f((BOARD_X/2), -(BOARD_Y/2), 0.5);
        glVertex3f((BOARD_X/2), (BOARD_Y/2), 0.5);
        glVertex3f((BOARD_X/2), (BOARD_Y/2), 0);
    glEnd();
    // Tył
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(1, 1, 1);
        glVertex3f(-(BOARD_X/2), -(BOARD_Y/2), 0);
        glVertex3f(-(BOARD_X/2), -(BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), (BOARD_Y/2), 0.5);
        glVertex3f(-(BOARD_X/2), (BOARD_Y/2), 0);
    glEnd();

    // Rysowanie czarnych kafelków szachownicy
    for(int i = -(BOARD_X/2); i < (BOARD_X/2); ++i)
    {
        for(int j = -(BOARD_Y/2); j < (BOARD_Y/2); j += 2)
        {
            if(i % 2 == 0)
                drawBlackField(i, j);
            else
                drawBlackField(i, j + 1);
        }
    }
}
/*!
 * \brief Rysuje czarne pole na planszy (na danych współrzędnych)
 *
 * Funkcja wywoływana w zagnieżdżonej pętli rysuje czarne pola na planszy tworząc
 * szachownicę. Pole jest kwadratem o wymiarach 1x1 [cm].
 *
 * \param x_position - współrzędna X lewego dolnego narożnika pola
 * \param y_position - współrzędna Y lewego dolnego narożnika pola
 * \param z_position - wysokość, na której umieszczane jest pole (wysokość planszy)
 */
void drawBlackField(GLfloat x_position, GLfloat y_position, GLfloat z_position)
{
    glBegin(GL_POLYGON);
        glNormal3f(0, 0, 1);
        glColor3f(0, 0, 0);
        glVertex3f(x_position, y_position, z_position);
        glVertex3f(x_position + 1, y_position, z_position);
        glVertex3f(x_position + 1, y_position + 1, z_position);
        glVertex3f(x_position, y_position + 1, z_position);
    glEnd();
}
/*!
 * \brief Rysuje prostopadłościan na podstawie danych współrzędnych
 *
 * \param x_coords - tablica współrzędnych X punktów prostopadłościanu
 * \param y_coords - tablica współrzędnych Y punktów prostopadłościanu
 * \param z_coords - tablica współrzędnych Z punktów prostopadłościanu
 * \param color - kolor bryły (domyślnie nullptr)
 * \param norm - wektor normalny (domyślnie nullptr)
 */
void drawPolygon(GLfloat *x_coords, GLfloat *y_coords, GLfloat *z_coords, const GLfloat *color, const GLfloat *norm)
{
    glBegin(GL_POLYGON);
        if(norm == nullptr)
            glNormal3f(0, 0, 1);
        else
            glNormal3fv(norm);

        if(color == nullptr)
            glColor3f(0, 0, 0);
        else
            glColor3fv(color);

        for(int i = 0; i < 4; ++i)
            glVertex3f(x_coords[i], y_coords[i], z_coords[i]);
    glEnd();
}
/*!
 * \brief Aktualizuje współrzędne w tablicy
 *
 * Mając wskaźnik na początek tablicy, nadpisuje zawarte w niej punkty
 * czterema nowymi danymi jako argumenty.
 *
 * \param[in] array - tablica współrzędnych
 * \param[in] point1 - pierwsza współrzędna
 * \param[in] point2 - druga współrzędna
 * \param[in] point3 - trzecia współrzędna
 * \param[in] point4 - czwarta współrzędna
 */
void updateArray(float *array, float point1, float point2, float point3, float point4)
{
    array[0] = point1;
    array[1] = point2;
    array[2] = point3;
    array[3] = point4;
}
/*!
 * \brief Rysuje przeszkodę w postaci walca
 *
 * Mając współrzędne X i Y przeszkody, rysowany jest walec o środku w punkcie
 * danym tymi współrzędnymi o promieniu 0.2 cm oraz wysokości 2.1 cm. Walec
 * zaślepiany jest od góry kołem.
 */
void drawObstacleLine(std::pair<float, float> coords, const float *color)
{
    float x_position;
    float y_position;
    float sinus = 0;
    float cosinus = 1;
    float height = 0.5 + OBSTACLE_HEIGHT;

    glBegin(GL_QUAD_STRIP);
        glColor3fv(color);

        for(double ang_deg = 0; ang_deg <= 360; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.2 * sinus;
            y_position = 0.2 * cosinus;

            glNormal3f(x_position + coords.first, y_position + coords.second, 0.5);
            glVertex3f(x_position + coords.first, y_position + coords.second, 0.5);
            glNormal3f(x_position + coords.first, y_position + coords.second, 0.5);
            glVertex3f(x_position + coords.first, y_position + coords.second, height);
        }
    glEnd();

    glBegin(GL_TRIANGLE_STRIP);
        glColor3fv(color);

        for(double ang_deg = 0; ang_deg <= 360; ang_deg +=  ANG_STEP_DEG)
        {
            sinus = sin(deg2rad(ang_deg));
            cosinus = cos(deg2rad(ang_deg));
            x_position = 0.2 * sinus;
            y_position = 0.2 * cosinus;

            glVertex3f(coords.first, coords.second, height);
            glNormal3f(x_position + coords.first, y_position + coords.second, height);
            glVertex3f(x_position + coords.first, y_position + coords.second, height);
        }
    glEnd();
}
