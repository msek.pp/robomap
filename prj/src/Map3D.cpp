#include "Map3D.hpp"

/*!
 * \file
 * \brief Definicje metod klasy Map3D
 */

/*!
 * \brief Inicjalizuje obiekt klasy Map3D
 *
 * Wymusza wywołanie konstruktora klasy FramedWidget, przekazując
 * mu tytuł widgetu oraz wskaźnik na widget będący rodzicem. Tworzy
 * dynamicznie przechowywane atrybuty i wywołuje metody, które
 * przygotowywują widgety do wyświetlenia w oknie głównym (pod-
 * danie elementów zarządcom geometrii).
 *
 * \param[in] map_title - tytuł widget'u wyświetlany na samej górze
 * \param[in] parent - wskaźnik na widget będący rodzicem
 */
Map3D::Map3D(const char *map_title, QWidget *parent) : FramedWidget(map_title, parent)
{
    map = new Viewer();
    map_container = new QWidget(this);
    map_container_layout = new QVBoxLayout(map_container);
    // Definicja zachowania geometrii
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    setStoredWidget();
    setVLayout();
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Definiuje zachowanie się geometrii kontenera z oknem
 * do wyświetlania grafiki.
 */
void Map3D::setStoredWidget()
{
    map_container_layout->addWidget(map);
    map_container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    map->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}
/*!
 * \brief Ustawia widget'y pochodne w pionie
 *
 * Idąc od góry, ustawiany jest tytuł a następnie kontener z oknem.
 */
void Map3D::setVLayout()
{
    vlayout->addWidget(title);
    vlayout->addWidget(map_container);
}
