#include "NumericData.hpp"

/*!
 * \file
 * \brief Definicje metod klasy NumericData
 */

/*!
 * \brief Inicjalizuje obiekt klasy NumericData
 *
 * Wymusza wywołanie konstruktora klasy QWidget przekazując
 * wskaźnik na rodzica. Tworzy obiekt QLineEdit, w którym
 * zapisywane będą odczyty z czujnika z dokładnością do
 * 1 milimetra. Wyołuje metody przygotowywujące przechowywane
 * widgety do prawidłowego wyświetlenia.
 *
 * \param[in] parent - wskaźnik na rodzica
 */
NumericData::NumericData(QWidget *parent) : QWidget(parent)
{
    // Tworzy obiekt klasy QLineEdit oraz zarządcę horyzontalnego
    data = new QLineEdit(this);
    hlayout = new QHBoxLayout(this);
    // Przygotowywuje widgety do wyświetlenia
    setStoredWidget();
    setHLayout();
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Ustawia odpowiednie parametry klasy QLineEdit, m.in.:
 * maksymalną długość wyświetlanego tekstu, zachowanie się
 * geometrii oraz wpisuje tekst początkowy.
 */
void NumericData::setStoredWidget()
{
    // Ustawienia klasy QLineEdit
    data->setReadOnly(true);
    data->setFrame(true);
    data->setMaxLength(TEXT_LIMIT);
    data->setTextMargins(1, 1, 1, 1);
    data->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    data->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    data->insert("0000 mm");

    data->setStyleSheet("color: white;");
}
/*!
 * \brief Ustawia widgety w poziomie
 *
 * Poddaje obiekt QLineEdit zarządcy horyzontalnemu
 */
void NumericData::setHLayout()
{
    hlayout->addWidget(data);
}
/*!
 * \brief Aktualizuje odczyt z czujnika odleŋłości
 *
 * \param[in] new_value - nowy odczyt z czujnika otrzymany z portu szeregowego
 */
void NumericData::update(unsigned int new_value)
{
    // Tworzy nowy napis do wyświetlenia
    QString printable = QStringLiteral("%1").arg(new_value);
    // Czyści aktualną zawartość
    data->clear();
    // Wstawia odczyt razem z jednostką
    data->insert(printable + " mm");
}
