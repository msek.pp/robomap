#include "SerialPort.hpp"

/*!
 * \file
 * \brief Definicje metod klasy SerialPort
 */

/*!
 * \brief Inicjalizuje obiekt klasy SerialPort
 *
 * Konstruktor domyślny.
 */
SerialPort::SerialPort() { }
/*!
 * \brief Otwiera port szeregowy
 *
 * Otwiera port szeregowy w trybie ReadOnly. Jeżeli
 * operacja zostanie przeprowadzona pomyślnie, ustawiane
 * są odpowiednie parametry komunikacji:
 * \n- BaudRate = 115200
 * \n- StopBits = 1
 * \n- DataBIts = 8
 * \n- Parity = None
 *
 * \return Zwraca true, jeżeli port został otworzony i skonfigurowany
 *         lub false w przeciwnym wypadku.
 */
bool SerialPort::startConnection()
{
    bool serial_opened = false;

    port_desc = open("/dev/rfcomm0", O_RDWR | O_NONBLOCK);

    if(port_desc > 0)
    {
        SetTransParam(port_desc);

        keep_reading = true;
        serial_opened = true;
    }

    return serial_opened;
}
/*!
 * \brief Kończy połączenie z portem szeregowym
 *
 * Zamyka otwarty deskryptor i powoduje zakończenie pracy
 * wątku, dzięki ustawieniu flagi keep_reading na fałsz.
 */
bool SerialPort::endConnection()
{
    close(port_desc);
    keep_reading = false;
    return true;
}
/*!
 * \brief Czyta dane z portu
 *
 * Tworzy tablicę na ramkę danych a następnie wczytuje
 * ramkę z portu. Ramkę przekazuje do funkcji parseDataFrame()
 * , która zwraca true jeżeli ramka jest prawidłowa lub false
 * jeżeli jest niekompletna lub błędnie odczytana.
 *
 * \param[in] data - obiekt przechowywujący wszystkie dane robota
 *
 * \return Zwraca true jeżeli czytanie i aktualizacja danych została
 *         przeprowadzona pomyślnie lub false w przeciwnym wypadku.
 */
bool SerialPort::receiveData()
{
    if(port_desc < 0)
        return false;

    std::string data_frame;
    bool read_status = false;

    if(RS232_Odbierz(returnDecriptor(), data_frame, FRAME_LIMIT, 100))
    {
        data_frame[data_frame.length()] = '\0';
        char *new_frame = new char[data_frame.length() + 1];
        std::copy(data_frame.begin(), data_frame.end(), new_frame);
        read_status = parseDataFrame(new_frame, &data, data_frame.length());
    }

    return read_status;
}
/*!
 * \brief Analizuje otrzymaną ramkę danych
 *
 * Ramkę w postaci tablicy rzutuje na strumień danych.
 * Sprawdzana jest poprawność ramki (pierwszy znak) a
 * następnie dane zostają wczytane i aktualizowane w
 * obiekcie RobotData.
 *
 * \param[in] data_frame - ramka danych
 * \param[in] data - obiekt przechowywujący wszystkie dane robota
 *
 * \return Zwraca true jeżeli dane zostały wczytane poprawnie lub false
 *         jeżeli znak startu ramki sie nie zgadzał lub nastąpił błąd
 *         strumienia wejściowego.
 */
bool parseDataFrame(const char *data_frame, RobotData *data, int data_length)
{
    // Rzutowanie na strumień wejściowy
    std::istringstream readed_line(data_frame);
    char header;
    // Wczytanie znaku startu
    readed_line >> header;

    // Sprawdzenie czy początek linii się zgadza
    if(header != 'X' || readed_line.fail() || data_length <= 1)
        return false;

    int current_data = 0;
    // Zczytanie danych z czujników odległości
    for(int i = 0; i < 4; ++i)
    {
        readed_line >> current_data;
        data->getDistance(current_data, i);
    }
    // Zczytanie danych z transoptorów
    for(int i = 0; i < 2; ++i)
    {
        readed_line >> current_data;
        data->getCounter(current_data, i);
    }
    // Zczytanie prędkości kół
    for(int i = 0; i < 2; ++i)
    {
        readed_line >> current_data;
        data->getVelocity(current_data, i);
    }
    // Sprawdzenie CRC
    char crc;
    readed_line >> std::hex >> crc;

    // Ponowne sprawdzenie poprawności wczytania
    if(readed_line.fail() || !checkCRC(const_cast<char*>(data_frame), data_length, crc))
        return false;

    return true;
}
/*!
 * \brief Sprawdza zgodność sumy kontrolnej CRC
 *
 * Oczyszcza ramkę danych wydobywając tylko te użyteczne
 * wartości a następnie liczy dla takiej tablicy sumę
 * kontrolną, która to jest porównywana z otrzymaną w ramce.
 */
bool checkCRC(char *data_frame, int data_length, char crc)
{
    char clear_data[data_length - 5];
    std::copy(&data_frame[2], &data_frame[data_length - 3], clear_data);
    clear_data[data_length - 3] = '\0';
    // Tablica clear_data teraz zawiera wszystkie użyteczne dane ze znakiem terminatora
    char new_crc = crc8(clear_data, std::strlen(clear_data) - 1);

    if(new_crc != crc)
        return false;

    return true;
}
/*!
 * \brief Oblicza sumę kontrolną otrzymanej ramki danych
 */
char crc8(char *data_frame, int data_length)
{
  char *data = data_frame;
  char crc = 0x00;
  char extract;
  char sum;
  for(int i = 0; i <= data_length; ++i)
  {
    extract = *data;
    for (char tempI = 8; tempI; tempI--)
    {
      sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if(sum)
      crc ^= 0x8C;
      extract >>= 1;
    }
    data++;
  }
  return crc;
}
/*!
 * \brief Funkcja wykonywana przez wątek czytający
 *
 * Wątek rozpoczyna pracę w funkcji "main" i czeka na sygnał
 * od zmiennej warunkowej. Po otrzymaniu sygnału czyta dane z
 * pliku, dopóki nie nastąpi zamknięcie deskryptora.
 */
void process(SerialPort *serial)
{
    std::unique_lock<std::mutex> lock(cm);
    // Wątek czeka na sygnał od zmiennej kondycyjnej
    start.wait(lock);

    while(serial->keepReading())
        serial->receiveData();
}






