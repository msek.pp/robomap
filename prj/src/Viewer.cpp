#include "Viewer.hpp"

/*!
 * \file
 * \brief Definicje metod klasy Viewer
 */

/*!
 * \brief Funkcja rysująca obiekty
 *
 * Ponownie odrysowywuje wszystkie elementy znajdujące się na
 * scenie.
 */
void Viewer::draw()
{
    robot.build();
}
/*!
 * \brief Funkcja inicjalizująca okno do wyświetlania grafiki
 */
void Viewer::init()
{
  setBackgroundColor(QColor(0x608da2));
  showEntireScene();
  // Restore previous viewer state.
  restoreStateFromFile();
}

