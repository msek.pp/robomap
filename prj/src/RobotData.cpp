#include "RobotData.hpp"

/*!
 * \file
 * \brief Definicje metod klasy RobotData
 */

/*!
 * \brief Inicjalizuje obiekt RobotData
 *
 * Nadaje wszystkim elementom tablic wartość 0.
 */
RobotData::RobotData()
{
    for(int i = 0; i < 4; ++i)
        sensors[i] = 0;

    for(int i = 0; i < 2; ++i)
        ktir_counters[i] = 0;

    for(int i = 0; i < 2; ++i)
        motors_velocity[i] = 0;
}

