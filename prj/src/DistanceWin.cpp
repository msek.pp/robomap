#include "DistanceWin.hpp"

/*!
 * \file
 * \brief Definicje metod klasy DistanceWin
 */

/*!
 * \brief Inicjalizuje widget DistanceWin
 *
 * Wymusza wywołanie konstruktora klasy FramedWidget, przekazując
 * mu tytuł widgetu oraz wskaźnik na widget będący rodzicem. Tworzy
 * dynamicznie przechowywane atrybuty i wywołuje metody, które
 * przygotowywują widgety do wyświetlenia w oknie głównym.
 *
 * \param[in] dist_title - tytuł widgetu wyświetlany na samej górze
 * \param[in] parent - wskaźnik na widget będący rodzicem
 */
DistanceWin::DistanceWin(const char *dist_title, QWidget *parent) : FramedWidget(dist_title, parent)
{
    // Utworzenie obrazu robota i położenie go na obiekcie DistanceWin
    image = new RobotImage(this);
    // Utworzenie obiektów klasy NumericData i zdefiniowanie zachowania się
    // ich geometrii
    for(int i = 0; i < SENSORS; ++i)
    {
        sensors_data[i] = new NumericData(image);
        sensors_data[i]->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    }
    // Utworzenie siatki kładzionej na obrazie robota
    image_grid = new QGridLayout(image);
    // Inicjalizacja przechowywanych widgetów
    setStoredWidget();
    // Ustawienie w pionie odpowiednich widgetów
    setVLayout();
    // Nadanie maksymalnej wysokości okna zdefiniowanej w makrze
    setMinimumHeight(WIN_HEIGHT);
    // Definicja zachowania geometrii
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Dodaje obiekty klasy NumericData do zarządcy geometrii na planie siatki.
 * Ustawia lokalizację i zachowanie geometrii zarządcy oraz obrazu robota.
 */
void DistanceWin::setStoredWidget()
{
    // Układanie okienek z odczytami na obrazie
    image_grid->addWidget(sensors_data[0], 1, 0, Qt::AlignHCenter | Qt::AlignVCenter);
    image_grid->addWidget(sensors_data[1], 0, 1, Qt::AlignHCenter | Qt::AlignVCenter);
    image_grid->addWidget(sensors_data[2], 0, 2, Qt::AlignHCenter | Qt::AlignVCenter);
    image_grid->addWidget(sensors_data[3], 1, 3, Qt::AlignHCenter | Qt::AlignVCenter);
    // Wyrównanie siatki do góry obrazu
    image_grid->setAlignment(Qt::AlignTop);
    // Definicja zachowania się geometrii obrazu
    image->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
}
/*!
 * \brief Ustawia widgety pochodne w pionie
 *
 * Idąc od góry, ustawiany jest tytuł a następnie obrazek.
 */
void DistanceWin::setVLayout()
{
    vlayout->addWidget(title);
    vlayout->addWidget(image);
}
