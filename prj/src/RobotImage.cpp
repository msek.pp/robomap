#include "RobotImage.hpp"

/*!
 * \file
 * \brief Definicje metod klasy RobotImage
 */

/*!
 * \brief Inicjalizuje obiekt klasy RobotImage
 *
 * Wywołuje konstruktor klasy bazowej QWidget przekazując
 * wskaźnik na rodzica.
 *
 * \param[in] parent - wskaźnik na rodzica
 */
RobotImage::RobotImage(QWidget *parent) : QWidget(parent) { }
/*!
 * \brief Rysuje obraz robota
 *
 * Tworzy obiekt QImage przekazując bezwzględną ścieżkę do obrazu
 * zdefiniowaną w makrze. Skaluje obraz do aktualnych rozmiarów widgetu
 * natychmiastowo (FastTransformation) bez zachowania stosunku boków
 * (IgnoreAspectRatio). Tak przeskalowany obraz umieszcza w lewym górnym
 * rogu (punkt [0, 0]).
 *
 * \param[in] event - wydarzenie mówiące o konieczności aktualizacji widgetu, np.
 *                przez zmianę rozmiaru okna
 */
void RobotImage::paintEvent(QPaintEvent *event)
{
    QPainter drawer(this);
    QImage robot_image(IMAGE);
    robot_image = robot_image.scaled(width(), height(), Qt::IgnoreAspectRatio, Qt::FastTransformation);
    drawer.drawImage(0, 0,robot_image);
}
