#include "VelocityGraph.hpp"

/*!
 * \file
 * \brief Definicje metod klasy VelocityGraph
 */

/*!
 * \brief Inicjalizuje obiekt klasy VelocityGraph
 *
 * Wymusza wywołanie konstruktora klasy FramedWidget, przekazując
 * mu tytuł widgetu oraz wskaźnik na widget będący rodzicem. Tworzy
 * dynamicznie przechowywane atrybuty i wywołuje metody, które
 * przygotowywują widgety do wyświetlenia w oknie głównym.
 *
 * \param[in] graph_title - tytuł widgetu
 * \param[in] parent - wskaźnik na rodzica
 */
VelocityGraph::VelocityGraph(const char *graph_title, QWidget *parent) : FramedWidget(graph_title, parent)
{
    graph = new QCustomPlot();

    setStoredWidget();
    setVLayout();
    // Ustawienie minimalnej wysokości, aby przy zmianach
    // rozmiaru okna głównego wykres był nadal czytelny
    setMinimumHeight(GRAPH_HEIGHT);
    // Definicja zachowania geometrii
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Konfiguruje wykres 2D, głównie pod względem wyglądowym.
 * Tytuły osi nie są wyświetlane, ponieważ zajmują dużo
 * miejsca w oknie wykresu. Na koniec ustawiany jest
 * gradient tła - metoda setPlotGradient().
 */
void VelocityGraph::setStoredWidget()
{
    graph->addGraph();

    graph->yAxis->setRange(-1, 1);
    graph->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    graph->xAxis->grid()->setVisible(false);
    graph->xAxis2->setVisible(true);
    graph->yAxis2->setVisible(true);
    graph->xAxis2->setTickLabels(false);
    graph->yAxis2->setTickLabels(false);
    graph->xAxis2->setTicks(false);
    graph->yAxis2->setTicks(false);
    graph->xAxis2->setSubTicks(false);
    graph->yAxis2->setSubTicks(false);
    graph->xAxis->setBasePen(QPen(Qt::white, 1));
    graph->yAxis->setBasePen(QPen(Qt::white, 1));
    graph->xAxis2->setBasePen(QPen(Qt::white, 1));
    graph->yAxis2->setBasePen(QPen(Qt::white, 1));
    graph->xAxis->setTickPen(QPen(Qt::white, 1));
    graph->yAxis->setTickPen(QPen(Qt::white, 1));
    graph->xAxis->setSubTickPen(QPen(Qt::white, 1));
    graph->yAxis->setSubTickPen(QPen(Qt::white, 1));
    graph->xAxis->setTickLabelColor(Qt::white);
    graph->yAxis->setTickLabelColor(Qt::white);
    graph->xAxis->setLabelColor(Qt::white);
    graph->yAxis->setLabelColor(Qt::white);
    graph->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));

    graph->graph(0)->setPen(QPen(QColor(200, 220, 255), 3));

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%m:%s");
    graph->xAxis->setTicker(timeTicker);

    connect(graph->xAxis, SIGNAL(rangeChanged(QCPRange)), graph->xAxis2, SLOT(setRange(QCPRange)));
    connect(graph->yAxis, SIGNAL(rangeChanged(QCPRange)), graph->yAxis2, SLOT(setRange(QCPRange)));

    setPlotGradient();
}
/*!
 * \brief Ustawia widgety pochodne w pionie
 *
 * Idąc od góry, ustawiany jest tytuł a potem wykres.
 */
void VelocityGraph::setVLayout()
{
    vlayout->addWidget(title);
    vlayout->addWidget(graph);
}
/*!
 * \brief Ustawia gradient tła wykresu
 *
 * Tworzy obiekt klasy QGradient przekazując już
 * zdefiniowany gradient SolidStone i ustawia go jako
 * tło wykresu.
 */
void VelocityGraph::setPlotGradient()
{
    QGradient plot_gradient(QGradient::SolidStone);
    graph->setBackground(plot_gradient);
}
/*!
 * \brief Aktualizuje dane na wykresie i przesuwa osie zgodnie z czasem
 *
 * Wywoływane za każdym razem, gdy główny timer skończy odliczanie. Stały
 * interwał w [ms] przekazywany jest jako drugi argument. Prędkość koła
 * jako wypełnienie PWM przerabiana jest na procentową wartość zgodnie
 * z maksymalnym zakresem zdefiniowanym jako MAX_RANGE w pliku nagłówkowym.
 */
void VelocityGraph::updateWheelVelocity(int velocity, int time_interval_ms)
{
    if(velocity > MAX_RANGE)
        velocity = MAX_RANGE;
    else if(velocity < -MAX_RANGE)
        velocity = -MAX_RANGE;

    double percent = ((double)velocity) / MAX_RANGE;

    double key = (double)time_interval_ms/1000.0;
    time_elapsed += key;

    graph->graph(0)->addData(time_elapsed, percent);
    graph->xAxis->setRange(time_elapsed, 8, Qt::AlignRight);
    graph->replot();
}
