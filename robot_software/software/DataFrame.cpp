#include "DataFrame.h"

void DataFrame::addDataToFrame(int new_data)
{
  char temp[10];

  sprintf(temp, "%d", new_data);

  int temp_index = 0;
  while(temp_index < 10)
  {
    if(temp[temp_index] == '\0')
      break;

    data_frame[index] = temp[temp_index];
    
    ++index;
    ++temp_index;
  }
}

void DataFrame::addSpace()
{
  data_frame[index] = ' ';
  ++index;
}

char DataFrame::crc8()
{
  char *data = data_frame;
  char crc = 0x00;
  char extract;
  char sum;
  for(int i = 0; i <= index; ++i)
  {
    extract = *data;
    for (char tempI = 8; tempI; tempI--) 
    {
      sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if(sum)
        crc ^= 0x8C;
      extract >>= 1;
    }
    data++;
  }
  return crc;
}

void DataFrame::prepareDataToSend(int *new_data)
{
  for(int i = 0; i < 7; ++i)
  {
    addDataToFrame(new_data[i]);
    addSpace();
  }
  addDataToFrame(new_data[7]);
  data_frame[index] = '\0';
}


