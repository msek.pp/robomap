#include "Robot.h"

#define LED 9
#define BUTTON 0
#define MEASURES 1

bool button_state = false;
uint32_t start_time;

void waitForButtonPressed();
void blinkLED(int blink, int time = 300);
void waitForBTConnection();

Robot minisumo;

void setup()
{
  pinMode(LED, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  digitalWrite(LED, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  // Serial port init
  Serial.begin();
  // Get all TOF sensors ready
  minisumo.setup();
  // Setup Bluetooth serial profile
  SerialBT.begin();

  start_time = millis();
}

void loop()
{
  while(!SerialBT)
  {
    blinkLED(5, 100);
    delay(500);
    minisumo.stop();
  }

  waitForButtonPressed();

  // ~~~~~~~~~~ Poruszanie się do przodu z interwałami 0.5s  
  minisumo.moveHalfSecond();
  
  start_time = millis();
  while(millis() - start_time <= 500)
  {
    minisumo.collectAllData();
    minisumo.sendData();
  }

  minisumo.turnBackIfObstacle(100);
/*
  // ~~~~~~~~~~~ Skanowanie całej przeszkody
  if(minisumo.detectObstacle(100))
  {
    minisumo.scanObstacle(100);
    button_state = false;
  }

  minisumo.collectAllData();
  minisumo.sendData();
  */
}

void waitForButtonPressed()
{
  while(!button_state)
  {
    if(millis() - start_time >= 300)
    {
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      start_time = millis();
    }

    if(digitalRead(BUTTON) == LOW)
    {
      button_state = true;
      delay(500);
      start_time = millis();
    }
  }
  //button_state = false;
}

void blinkLED(int blinks, int time)
{
  for(int i = 0; i < blinks; ++i)
  {
    digitalWrite(LED, HIGH);
    delay(time);
    digitalWrite(LED, LOW);
    delay(time);
  }
}

void waitForBTConnection()
{
  while(!SerialBT)
  {
    blinkLED(5, 100);
    delay(500);
    minisumo.stop();
  }
  //start_time = millis();
}



