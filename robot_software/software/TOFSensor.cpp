#include "TOFSensor.h"

void TOFSensor::initSensor(pin_size_t pin, const char *sensor_name, TwoWire *i2c_address)
{
  xshut = pin;
  //name = sensor_name;
  pinMode(xshut, OUTPUT);
  sensor.setBus(i2c_address);
}

bool TOFSensor::setup()
{
  uint32_t start = millis();
  uint32_t end = 0;
  bool init_done = true;

  while(true)
  {
    if(!sensor.init())
    {
      Serial.print("Cannot init ");
      Serial.print(name);
      Serial.println(" sensor");
      delay(500);
    }
    else
    {
      init_done = true;
      break;
    }
    // Sensor initialization timeout
    end = millis();
    if(end - start > INIT_TIMEOUT)
    {
      init_done = false;
      break;
    }
  }
  return init_done;
}

uint16_t TOFSensor::returnMeanMeasure()
{
  uint16_t meaned = 0;
  for(int i = 0; i < 4; ++i)
    meaned += sensor.readRangeContinuousMillimeters();

  return (meaned/4);
}





