#include "DCMotors.h"

void DCMotors::setupMotors()
{
  motors[0].setupMotor(INA1, INA2, PWMA);
  motors[1].setupMotor(INB1, INB2, PWMB);
}

int DCMotors::motorState()
{
  if(motors[0].returnState() == state::FORWARD || motors[1].returnState() == state::FORWARD)
    return state::FORWARD;
  else if(motors[0].returnState() == state::STOP && motors[1].returnState() == state::STOP)
    return state::STOP;
  else
    return state::BACKWARD;
}

void DCMotors::goBackward(uint16_t velocity)
{
  motors[0].move(state::BACKWARD, velocity);
  motors[1].move(state::BACKWARD, velocity);  
}

void DCMotors::goForward(uint16_t velocity)
{
  motors[0].move(state::FORWARD, velocity);
  motors[1].move(state::FORWARD, velocity);  
}

void DCMotors::moveMotors(int velocity_1, int velocity_2)
{
  if(velocity_1 > 0)
    motors[0].move(state::FORWARD, velocity_1);
  else if(velocity_1 == 0)
    motors[0].stop();
  else
    motors[0].move(state::BACKWARD, -velocity_1);

  if(velocity_2 > 0)
    motors[1].move(state::FORWARD, velocity_2);
  else if(velocity_2 == 0)
    motors[1].stop();
  else
    motors[1].move(state::BACKWARD, -velocity_2);
}

void DCMotors::stopMotors()
{
  motors[0].stop();
  motors[1].stop();
}

void DCMotors::getVelocities()
{
  motors_velocity[0] = motors[1].returnVelocity();
  motors_velocity[1] = motors[0].returnVelocity();
}

