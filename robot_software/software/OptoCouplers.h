#ifndef OPTOCOUPLERS_H
#define OPTOCOUPLERS_H

#include <stdio.h>
#include <Arduino.h>

#define KTIR_1 27
#define KTIR_2 26

#define KTIR 2

class OptoCouplers
{
  public:
    uint16_t opto_reads[KTIR];
    
    OptoCouplers() { }
    void setupOptoCouplers();
    void getOptoReads();
};

#endif // OPTOCOUPLERS_H