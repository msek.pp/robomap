#ifndef TOFSENSOR_H
#define TOFSENSOR_H

// Library for TOF sensors
#include <VL53L0X.h>
#include <stdlib.h>

#define INIT_TIMEOUT 2000

class TOFSensor
{
  private:
    char *name;
    pin_size_t xshut;
    VL53L0X sensor;

  public:
    TOFSensor() { }
    ~TOFSensor();

    bool setup();
    void initSensor(pin_size_t pin, const char *sensor_name, TwoWire *i2c_address);

    void turnOn() { digitalWrite(xshut, HIGH); }
    void turnOff() { digitalWrite(xshut, LOW); }
    void setTimeout(int timeout) { sensor.setTimeout(timeout); }
    void setAddress(uint8_t sensor_address) { sensor.setAddress(sensor_address); }
    void start() { sensor.startContinuous(); }
    uint16_t returnMeasure() { return sensor.readRangeContinuousMillimeters(); }
    uint16_t returnMeanMeasure();
    const char * returnName() { return name; }
};

#endif // TOFSENSOR_H