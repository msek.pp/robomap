#ifndef DATAFRAME_H
#define DATAFRAME_H

#include <stdio.h>
#include <Arduino.h>
#include <SerialBT.h>

#define FRAME_LENGTH 40
#define POLYNOMIAL 0x0161

class DataFrame
{
  private:
    unsigned int index = 0;

    void addSpace();

  public:
    char data_frame[FRAME_LENGTH];

    DataFrame() { }

    void addDataToFrame(int new_data);
    char crc8();
    void prepareDataToSend(int *new_data);
    void restartFrame() { index = 0; }
};

#endif // DATAFRAME_H