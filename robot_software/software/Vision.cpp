#include "Vision.h"

uint8_t addresses[] = {0x30, 0x32, 0x34, 0x36};

void Vision::initSensors()
{
  tof_sensors[0].initSensor(XSHUT_LEFT, "left sided", &Wire);
  tof_sensors[1].initSensor(XSHUT_FRONT1, "front left", &Wire);
  tof_sensors[2].initSensor(XSHUT_FRONT2, "front right", &Wire1);
  tof_sensors[3].initSensor(XSHUT_RIGHT, "right sided", &Wire1);

  setSensorsTimeout();
}

void Vision::setupI2C()
{
  // Set correct pins for I2C0 and I2C1
  Wire.setSDA(I2C0_SDA);
  Wire.setSCL(I2C0_SCL);
  Wire1.setSDA(I2C1_SDA);
  Wire1.setSCL(I2C1_SCL);
  // Start I2C
  Wire.begin();
  Wire1.begin();
}

void Vision::setSensorsTimeout(int timeout)
{
  for(int i = 0; i < SENSORS; ++i)
    tof_sensors[i].setTimeout(timeout);
}

void Vision::restartSensors()
{
  // Shut down all sensors
  for(int i = 0; i < SENSORS; ++i)
    tof_sensors[i].turnOff();

  delay(1000);
  // Turn on all sensors
  for(int i = 0; i < SENSORS; ++i)
    tof_sensors[i].turnOn();

  delay(1000);
  // Shut down all sensors again
  for(int i = 0; i < SENSORS; ++i)
    tof_sensors[i].turnOff();
    
  delay(100);
}

void Vision::setupSensors()
{
  setupI2C();
  initSensors();
  restartSensors();

  for(int i = 0; i < SENSORS; ++i)
  {
    tof_sensors[i].turnOn();
    delay(25);
    tof_sensors[i].setAddress(addresses[i]);
    
    if(!tof_sensors[i].setup())
    {
      Serial.print(tof_sensors[i].returnName());
      Serial.println(" cannot be initialized - timeout error");
    }
  }
  startMeasure();
}

void Vision::startMeasure()
{
  for(int i = 0; i < SENSORS; ++i)
    tof_sensors[i].start(); 
}

void Vision::getDistances()
{
  for(int i = 0; i < SENSORS; ++i)
    clear_distances[i] = tof_sensors[i].returnMeasure();
}

void Vision::sendDistancesOnSerial()
{
  for(int i = 0; i < SENSORS; ++i)
  {
    Serial.print(clear_distances[i]);
    Serial.print(" ");  
  }
}