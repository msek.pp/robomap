#include "Robot.h"

void Robot::setup()
{
  surr_vision.setupSensors();
  board_vision.setupOptoCouplers();
  motors.setupMotors();
}

void Robot::collectAllData()
{
  surr_vision.getDistances();
  board_vision.getOptoReads();
  motors.getVelocities();
}

bool Robot::sendData()
{
  int data_array[8];
  bool transmit_done = true;

  for(int i = 0; i < 4; ++i)
    data_array[i] = surr_vision.clear_distances[i];

  data_array[4] = board_vision.opto_reads[0];
  data_array[5] = board_vision.opto_reads[1];
  data_array[6] = motors.motors_velocity[0];
  data_array[7] = motors.motors_velocity[1];

  data_frame.prepareDataToSend(data_array);
  char crc = data_frame.crc8();

  if(!SerialBT || !SerialBT.availableForWrite())
    transmit_done = false;
  else
  {
    const char header[2] = {'X', ' '};
    char crc_end[4];

    SerialBT.write(header, 2);
    SerialBT.write(data_frame.data_frame, strlen(data_frame.data_frame));
    sprintf(crc_end, " %02x\n", crc);
    SerialBT.write(crc_end, 4);
  }

  data_frame.restartFrame();

  return transmit_done;
}

void Robot::move(double velocity, double omega)
{
  omega = omega*PI/180;

  double left_wheel = velocity - (WHEELS_L * omega)/2;
  double right_wheel = velocity + (WHEELS_L * omega)/2;

  if(left_wheel < 0)
    left_wheel = -(LINEAR_COEF * (-left_wheel) + OFFSET);
  else
    left_wheel = LINEAR_COEF * left_wheel + OFFSET;
  if(right_wheel < 0)
    right_wheel = -(LINEAR_COEF * (-right_wheel) + OFFSET);
  else
    right_wheel = LINEAR_COEF * right_wheel + OFFSET;
  
  motors.moveMotors(left_wheel, right_wheel);
}

bool Robot::detectObstacle(int range)
{
  bool obstacle_detected = false;

  surr_vision.getDistances();
  if((surr_vision.clear_distances[1] > range) || (surr_vision.clear_distances[2] > range))
    move(4, 0);
  else
  {
    stop();
    obstacle_detected = true;
  }
  collectAllData();
  sendData();
  return obstacle_detected;
}

void Robot::turnBackIfObstacle(int range)
{
  surr_vision.getDistances();
  if((surr_vision.clear_distances[1] <= range) && (surr_vision.clear_distances[2] <= range))
    rotate(90, 2000);

  collectAllData();
  sendData();
}

void Robot::scanObstacle(int range)
{
  // Obrót, skan boku przeszkody i przejazd kawałek dalej dla swobodniejszego obrotu
  rotate(-90, 1000);
  for(int i = 0; i < 5; ++i)
  {
    surr_vision.getDistances();
    while(surr_vision.clear_distances[3] <= range + 40)
    {
      move(4, 0);
      collectAllData();
      sendData();
    }
    moveFwdNoDelay(4, 4000);
    rotate(90, 1000);
    moveFwdNoDelay(4, 2000);
  }
}

void Robot::rotate(int angle, int ms)
{
  uint16_t start_time = millis();

  move(0, angle);
  while(millis() - start_time <= ms)
  {
    collectAllData();
    sendData();
  }
  stop(); 
}

void Robot::moveFwdNoDelay(int distance, int ms)
{
  uint16_t start_time = millis();

  move(distance, 0);
  while(millis() - start_time <= ms)
  {
    collectAllData();
    sendData();
  }
  stop();
}

void Robot::moveOneCentimeter()
{
  board_vision.getOptoReads();
  bool left_opto = board_vision.opto_reads[0] > 600 ? 1 : 0;
  bool right_opto = board_vision.opto_reads[1] > 600 ? 1 : 0;
  
  uint16_t start_time = millis();
  move(3, 0);
  while(millis() - start_time <= 20000)
  {
    board_vision.getOptoReads();
    bool new_left_opto = board_vision.opto_reads[0] > 600 ? 1 : 0;
    bool new_right_opto = board_vision.opto_reads[1] > 600 ? 1 : 0;
    if((new_left_opto != left_opto) && (new_right_opto != right_opto))
      break;
    
    collectAllData();
    sendData();
  }
  stop();
}

void Robot::moveHalfSecond()
{
  uint16_t start_time = millis();
  move(2, 0);
  while(millis() - start_time <= 500)
  {
    collectAllData();
    sendData();
  }
  stop();
}



