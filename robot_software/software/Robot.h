#ifndef ROBOT_H
#define ROBOT_H

#include "DataFrame.h"
#include "Vision.h"
#include "OptoCouplers.h"
#include "DCMotors.h"

#define WHEELS_L 8.6
#define LINEAR_COEF 9.85
#define OFFSET 7.395194838

class Robot
{
  private:
    Vision surr_vision;
    OptoCouplers board_vision;
    DCMotors motors;
    DataFrame data_frame;

  public:
    Robot() { }
    void setup();
    void collectAllData();
    bool sendData();
    void move(double velocity, double omega);
    void stop() { motors.stopMotors(); }
    void moveForward(uint16_t velocity) { motors.goForward(velocity); }
    int state() { return motors.motorState(); }

    bool detectObstacle(int range);
    void scanObstacle(int range);
    void rotate(int angle, int ms);
    void moveFwdNoDelay(int distance, int ms);
    void moveOneCentimeter();
    void moveHalfSecond();
    void turnBackIfObstacle(int range);
};

#endif // ROBOT_H