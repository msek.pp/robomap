#include "OptoCouplers.h"

void OptoCouplers::setupOptoCouplers()
{
  analogReadResolution(12);
  pinMode(KTIR_1, INPUT);
  pinMode(KTIR_2, INPUT);
}

void OptoCouplers::getOptoReads()
{
  opto_reads[0] = analogRead(KTIR_1);
  opto_reads[1] = analogRead(KTIR_2);
}