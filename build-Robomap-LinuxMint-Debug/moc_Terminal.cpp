/****************************************************************************
** Meta object code from reading C++ file 'Terminal.hpp'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../prj/inc/Terminal.hpp"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Terminal.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSTerminalENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSTerminalENDCLASS = QtMocHelpers::stringData(
    "Terminal",
    "typeSerialOpenError",
    "",
    "typeSerialReadError",
    "typeSerialReady",
    "typeSerialEnded",
    "typeObstacleDetected",
    "typePlLang",
    "typeEngLang"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSTerminalENDCLASS_t {
    uint offsetsAndSizes[18];
    char stringdata0[9];
    char stringdata1[20];
    char stringdata2[1];
    char stringdata3[20];
    char stringdata4[16];
    char stringdata5[16];
    char stringdata6[21];
    char stringdata7[11];
    char stringdata8[12];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSTerminalENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSTerminalENDCLASS_t qt_meta_stringdata_CLASSTerminalENDCLASS = {
    {
        QT_MOC_LITERAL(0, 8),  // "Terminal"
        QT_MOC_LITERAL(9, 19),  // "typeSerialOpenError"
        QT_MOC_LITERAL(29, 0),  // ""
        QT_MOC_LITERAL(30, 19),  // "typeSerialReadError"
        QT_MOC_LITERAL(50, 15),  // "typeSerialReady"
        QT_MOC_LITERAL(66, 15),  // "typeSerialEnded"
        QT_MOC_LITERAL(82, 20),  // "typeObstacleDetected"
        QT_MOC_LITERAL(103, 10),  // "typePlLang"
        QT_MOC_LITERAL(114, 11)   // "typeEngLang"
    },
    "Terminal",
    "typeSerialOpenError",
    "",
    "typeSerialReadError",
    "typeSerialReady",
    "typeSerialEnded",
    "typeObstacleDetected",
    "typePlLang",
    "typeEngLang"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSTerminalENDCLASS[] = {

 // content:
      11,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,   56,    2, 0x0a,    1 /* Public */,
       3,    0,   57,    2, 0x0a,    2 /* Public */,
       4,    0,   58,    2, 0x0a,    3 /* Public */,
       5,    0,   59,    2, 0x0a,    4 /* Public */,
       6,    0,   60,    2, 0x0a,    5 /* Public */,
       7,    0,   61,    2, 0x0a,    6 /* Public */,
       8,    0,   62,    2, 0x0a,    7 /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject Terminal::staticMetaObject = { {
    QMetaObject::SuperData::link<FramedWidget::staticMetaObject>(),
    qt_meta_stringdata_CLASSTerminalENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSTerminalENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSTerminalENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<Terminal, std::true_type>,
        // method 'typeSerialOpenError'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'typeSerialReadError'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'typeSerialReady'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'typeSerialEnded'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'typeObstacleDetected'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'typePlLang'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'typeEngLang'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void Terminal::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Terminal *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->typeSerialOpenError(); break;
        case 1: _t->typeSerialReadError(); break;
        case 2: _t->typeSerialReady(); break;
        case 3: _t->typeSerialEnded(); break;
        case 4: _t->typeObstacleDetected(); break;
        case 5: _t->typePlLang(); break;
        case 6: _t->typeEngLang(); break;
        default: ;
        }
    }
    (void)_a;
}

const QMetaObject *Terminal::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Terminal::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSTerminalENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return FramedWidget::qt_metacast(_clname);
}

int Terminal::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = FramedWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
