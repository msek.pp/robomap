# RoboMap Project

Window application based on QT6 libraries written for course "Sensor's Data Visualization" at Wroclaw University Of Science and Technology.

## Objective

Application use physical mobile robot and it's four TOF sensors to draw simple map on 3D plot in real-time. Besides 3D plot, program has
multiple features:  
+ distance data from all four sensors
+ two velocity time graphs for each wheel
+ terminal for communication with robot
+ start button

It communicates with robot through Bluetooth as serial port simulation. Some example photos are placed in repo.

## Build

Run with QTCreator or:

+ cd prj
+ qmake Robomap.pro
+ make
+ ./Robomap

